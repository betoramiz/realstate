﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using RealState.Web.Infrastructure.Repositories;
using vm = RealState.Web.ViewModels;
using models = RealState.Web.Models;
using RealState.Web.Services.Implementation;
using Shouldly;
using Xunit;

namespace RealState.UniTest
{
    public class ParcelaTest
    {
        private Mock<IParcelaRepository> parcelaRepository;
        private Mock<IMapper> mapper;

        public ParcelaTest()
        {
            parcelaRepository = new Mock<IParcelaRepository>();
            mapper = new Mock<IMapper>();
        }

        [Fact]
        public async Task Show_All_Register()
        {
            var parcelasvm = new List<vm.Parcelas.Parcelas>();
            parcelasvm.Add(new vm.Parcelas.Parcelas() { Id = 1 });
            parcelasvm.Add(new vm.Parcelas.Parcelas() { Id = 2 });

            var parcelas = new List<models.Parcela>();
            parcelas.Add(new models.Parcela() { Id = 1 });
            parcelas.Add(new models.Parcela() { Id = 2 });


            //Arrange
            parcelaRepository.Setup(x => x.GetListAsync()).ReturnsAsync(parcelas);
            mapper.Setup(x => x.Map<IEnumerable<models.Parcela>, List<vm.Parcelas.Parcelas>>(It.IsAny<List<models.Parcela>>())).Returns(parcelasvm);

            //Act
            var service = new ParcelaService(parcelaRepository.Object, mapper.Object);
            var result = await service.GetListAsync();

            //Assert
            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            Assert.Equal(result.Count, parcelas.Count);
            parcelaRepository.Verify(x => x.GetListAsync(), Times.Once);
        }

        [Fact]
        public async Task Response_with_No_Data()
        {
            //Arrange
            parcelaRepository.Setup(x => x.GetListAsync()).ReturnsAsync(new List<models.Parcela>());
            mapper.Setup(x => x.Map<IEnumerable<models.Parcela>, List<vm.Parcelas.Parcelas>>(It.IsAny<List<models.Parcela>>())).Returns(new List<vm.Parcelas.Parcelas>());

            //Act
            var service = new ParcelaService(parcelaRepository.Object, mapper.Object);
            var result = await service.GetListAsync();

            //Assert
            result.ShouldNotBeNull();
            result.ShouldBeEmpty();
            Assert.Empty(result);
            parcelaRepository.Verify(x => x.GetListAsync(), Times.Once);
        }

        [Fact]
        public async Task Create_Parcela()
        {
            var parcelaCreate = new vm.Parcelas.ParcelaCreate() { Numero = "123" };
            var parcela = new List<models.Parcela>();

            parcelaRepository.Setup(x => x.GetByNumberAsync(It.IsAny<string>())).ReturnsAsync(parcela);
            mapper.Setup(x => x.Map<vm.Parcelas.ParcelaCreate, models.Parcela>(It.IsAny<vm.Parcelas.ParcelaCreate>()));

            //Act
            var service = new ParcelaService(parcelaRepository.Object, mapper.Object);
            await service.CreateAsync(parcelaCreate);

            //Assert
            parcelaRepository.Verify(x => x.Create(It.IsAny<models.Parcela>()), Times.Once);
        }

        [Fact]
        public async Task Create_Parcela_Repited_Number()
        {
            var parcelaCreate = new vm.Parcelas.ParcelaCreate() { Numero = "123" };
            var parcelaList = new List<models.Parcela>();
            parcelaList.Add(new models.Parcela() { Id = 1 });

            //Arrange
            parcelaRepository.Setup(x => x.GetByNumberAsync(It.IsAny<string>())).ReturnsAsync(parcelaList);
            mapper.Setup(x => x.Map<vm.Parcelas.ParcelaCreate, models.Parcela>(It.IsAny<vm.Parcelas.ParcelaCreate>()));

            //Act
            var service = new ParcelaService(parcelaRepository.Object, mapper.Object);

            //Assert
            await Assert.ThrowsAsync<Exception>(() => service.CreateAsync(parcelaCreate));
        }
    }
}
