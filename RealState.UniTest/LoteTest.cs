﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using RealState.Web.Infrastructure.Repositories;
using vm = RealState.Web.ViewModels;
using models = RealState.Web.Models;
using RealState.Web.Services.Implementation;
using Shouldly;
using RealState.Web.Services;
using Xunit;

namespace RealState.UniTest
{
    public class LoteTest
    {
        private Mock<ILoteRepository> loteRepository;
        private Mock<IMapper> mapper;

        [Fact]
        public async Task Find_Lote_By_Number()
        {
            loteRepository = new Mock<ILoteRepository>();
            mapper = new Mock<IMapper>();

            var loteList = new List<models.Lote>();
            loteList.Add(new models.Lote() { Id = 1, Numero = "numero 1"});
            loteList.Add(new models.Lote() { Id = 2, Numero = "numero 2" });
            loteList.Add(new models.Lote() { Id = 3, Numero = "numero 3" });

            var lotesVmlist = new List<vm.Lotes.Lotes>();
            lotesVmlist.Add(new vm.Lotes.Lotes() { Id = 1, Numero = "numero 1" });
            lotesVmlist.Add(new vm.Lotes.Lotes() { Id = 1, Numero = "numero 2" });
            lotesVmlist.Add(new vm.Lotes.Lotes() { Id = 1, Numero = "numero 3" });

            //Arrange
            loteRepository.Setup(x => x.GetByNumberAsync(It.IsAny<string>())).ReturnsAsync(loteList);
            mapper.Setup(x => x.Map<IEnumerable<models.Lote>, List<vm.Lotes.Lotes>>(It.IsAny<List<models.Lote>>())).Returns(lotesVmlist);

            //Act
            var anySearchTerm = "any search";
            var service = new LoteService(loteRepository.Object, mapper.Object);
            var result = await service.BuscarPorNumeroAsync(anySearchTerm);

            //Assert
            result.ShouldNotBeEmpty();
            result.ShouldBeSameAs(lotesVmlist);
            Assert.Equal(result.Count, lotesVmlist.Count);
            loteRepository.Verify(x => x.GetByNumberAsync(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task Find_Lote_By_Number_Emty_Results()
        {
            loteRepository = new Mock<ILoteRepository>();
            mapper = new Mock<IMapper>();

            //Arrange
            loteRepository.Setup(x => x.GetByNumberAsync(It.IsAny<string>())).ReturnsAsync(new List<models.Lote>());
            mapper.Setup(x => x.Map<IEnumerable<models.Lote>, List<vm.Lotes.Lotes>>(It.IsAny<List<models.Lote>>())).Returns(new List<vm.Lotes.Lotes>());

            //Act
            var service = new LoteService(loteRepository.Object, mapper.Object);
            var result = await service.BuscarPorNumeroAsync(It.IsAny<string>());

            //Assert
            result.ShouldBeEmpty();
            loteRepository.Verify(x => x.GetByNumberAsync(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task Get_Lote_By_Id()
        {
            loteRepository = new Mock<ILoteRepository>();
            mapper = new Mock<IMapper>();

            var lote = new models.Lote() { Id = 1, Numero = "123" };
            var loteVm = new vm.Lotes.Lotes() { Id = 1, Numero = "123" };

            //Arrange
            loteRepository.Setup(x => x.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(lote);
            mapper.Setup(x => x.Map<models.Lote, vm.Lotes.Lotes>(lote)).Returns(loteVm);

            //Act
            var service = new LoteService(loteRepository.Object, mapper.Object);
            var result = await service.GetByIdAsync(It.IsAny<int>());

            //Arrange
            result.ShouldNotBeNull();
            result.Id.ShouldBe(loteVm.Id);
            loteRepository.Verify(x => x.GetByIdAsync(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task Get_Lote_By_Id_Invalid_Should_Throw_Exception()
        {
            loteRepository = new Mock<ILoteRepository>();
            mapper = new Mock<IMapper>();

            //Arrange
            var service = new Mock<ILoteService>();
            service.Setup(x => x.GetByIdAsync(It.IsAny<int>())).Throws(new Exception());

            //Act & Asseert
            await Assert.ThrowsAsync<Exception>( () => service.Object.GetByIdAsync(It.IsAny<int>()));
        }

        [Fact]
        public async Task Create_Lote()
        {
            loteRepository = new Mock<ILoteRepository>();
            mapper = new Mock<IMapper>();

            var loteVm = new vm.Lotes.LoteCreate() { Numero = "123", Precio = 100, Ancho = 10, Largo = 10, ParcelaId = 1 };
            var lote = new models.Lote() { Id = 1, Numero = "123", Precio = 1, Largo = 10, Ancho = 10, ParcelaId = 1, Parcela = new models.Parcela(), Enable = true };

            //Arrage
            loteRepository.Setup(x => x.GetByNumberAsync(It.IsAny<string>())).ReturnsAsync(() => new List<models.Lote>());
            //mapper.Setup(x => x.Map<vm.Lotes.LoteCreate, models.Lote>(It.IsAny<vm.Lotes.LoteCreate>())).Returns(lote);
            loteRepository.Setup(x => x.Create(lote)).Callback(() => lote.Numero = "");

            //Act
            var service = new LoteService(loteRepository.Object, mapper.Object);
            await service.CreateAsync(loteVm);

            //Assert
            loteRepository.Verify(x => x.Create(It.IsAny<models.Lote>()), Times.Once);
        }

        [Theory]
        [InlineData("aaa111")]
        public async Task Create_Lote_Repited_Number(string loteNumber)
        {
            loteRepository = new Mock<ILoteRepository>();
            mapper = new Mock<IMapper>();
            var loteVm = new vm.Lotes.LoteCreate() { Numero = loteNumber, Precio = 100, Ancho = 10, Largo = 10, ParcelaId = 1 };
            var loteList = new List<models.Lote>();
            loteList.Add(new models.Lote() { Id = 1 });
            loteList.Add(new models.Lote() { Id = 2 });

            //Arrage
            loteRepository.Setup(x => x.GetByNumberAsync(It.IsAny<string>())).ReturnsAsync(loteList);
            mapper.Setup(x => x.Map<vm.Lotes.LoteCreate, models.Lote>(It.IsAny<vm.Lotes.LoteCreate>())).Returns(() => new models.Lote());

            var errorMessage = $"Ya existe un lote con el Numero {loteNumber}";
            var service = new LoteService(loteRepository.Object, mapper.Object);

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => service.CreateAsync(loteVm));
            var excepton = await Assert.ThrowsAsync<Exception>(() => service.CreateAsync(loteVm));

            excepton.Message.ShouldBe(errorMessage);
        }

        [Fact]
        public async Task Update_Lote()
        {
            loteRepository = new Mock<ILoteRepository>();
            mapper = new Mock<IMapper>();
            var loteVm = new vm.Lotes.LoteCreate() { Numero = "123" };
            var loteList = new List<models.Lote>();
            loteList.Add(new models.Lote() { Id = 1 });

            var lote = new models.Lote() { Numero = "123" };

            //Arrage
            loteRepository.Setup(x => x.GetByNumberAsync(It.IsAny<string>())).ReturnsAsync(loteList);
            //loteRepository.Setup(x => x.Update(lote));
            mapper.Setup(x => x.Map<vm.Lotes.LoteCreate, models.Lote>(It.IsAny<vm.Lotes.LoteCreate>())).Returns(lote);

            //Act
            var service = new LoteService(loteRepository.Object, mapper.Object);
            await service.UpdateAsync(loteVm);

            //Assert
            loteRepository.Verify(x => x.GetByNumberAsync(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task Update_Lote_Not_Existing()
        {
            loteRepository = new Mock<ILoteRepository>();
            mapper = new Mock<IMapper>();
            var loteVm = new vm.Lotes.LoteCreate() { Numero = "123" };

            //Arrage
            loteRepository.Setup(x => x.GetByNumberAsync(It.IsAny<string>())).ReturnsAsync(() => new List<models.Lote>());

            //Act
            var service = new LoteService(loteRepository.Object, mapper.Object);

            //Assert
            await Assert.ThrowsAsync<Exception>(() => service.UpdateAsync(loteVm));
        }

        [Fact]
        public async Task Update_Lote_Repited_LoteNumber()
        {
            loteRepository = new Mock<ILoteRepository>();
            mapper = new Mock<IMapper>();
            var loteVm = new vm.Lotes.LoteCreate() { Numero = "123" };
            var listaLotes = new List<models.Lote>();
            listaLotes.Add(new models.Lote() { Id = 1 });
            listaLotes.Add(new models.Lote() { Id = 2 });

            //Arrage
            loteRepository.Setup(x => x.GetByNumberAsync(It.IsAny<string>())).ReturnsAsync(listaLotes);

            //Act
            var service = new LoteService(loteRepository.Object, mapper.Object);

            //Assert
            await Assert.ThrowsAsync<Exception>(() => service.UpdateAsync(loteVm));
        }

        [Fact]
        public void Prueba()
        {
            //loteRepository = new Mock<ILoteRepository>();

            //loteRepository.Setup(x => x.Create(It.IsAny<models.Lote>()));

            //loteRepository.Object.Create(It.IsAny<models.Lote>());

            //loteRepository.Verify(x => x.Create(It.IsAny<models.Lote>()), Times.Once);
        }
    }
}
