﻿using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using RealState.Web.Infrastructure;
using RealState.Web.Infrastructure.Repositories;
using RealState.Web.Services;
using RealState.Web.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace RealState.Web.App_Start
{
    public class DIConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            //Repositories
            builder.RegisterType<Context>();
            builder.RegisterGeneric(typeof(RepositoryAsync<>)).As(typeof(IRepositoryAsync<>));
            builder.RegisterType<ParcelaRepository>().As<IParcelaRepository>();
            builder.RegisterType<LoteRepository>().As<ILoteRepository>();
            builder.RegisterType<ClienteRepository>().As<IClienteRepository>();
            builder.RegisterType<ContratoRepository>().As<IContratoRepository>();
            builder.RegisterType<PagoRepository>().As<IPagoRepository>();
            builder.RegisterType<OrdenPagoRepository>().As<IOrdenPagoRepository>();


            //Services
            builder.RegisterType<ParcelaService>().As<IParcelaService>();
            builder.RegisterType<LoteService>().As<ILoteService>();
            builder.RegisterType<ClienteService>().As<IClienteService>();
            builder.RegisterType<ContratoService>().As<IContratoService>();
            builder.RegisterType<PagoService>().As<IPagoService>();
            builder.RegisterType<BancoService>().As<IBancoService>();
            builder.RegisterType<OrdenPagoService>().As<IOrdenPagoService>();
            builder.RegisterType<WebhookService>().As<IWebhookService>();
            builder.RegisterType<TableroService>().As<ITableroService>();

            //Automapper
            builder.Register(x => Mapper.MappingConfig.InitializeAutoMapper()).AsImplementedInterfaces().SingleInstance();
            builder.Register(c => c.Resolve<IConfigurationProvider>().CreateMapper()).As<IMapper>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}