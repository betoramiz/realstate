﻿using Firebase.Auth;
using System.Runtime.Caching;
using System.Web.Mvc;
using System.Web.Routing;

namespace RealState.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //if (System.Web.HttpContext.Current.Session["FirebaseUser"] is User User)
            //{
            //    routes.MapRoute(
            //       name: "Default",
            //       url: "{controller}/{action}/{id}",
            //       defaults: new { controller = "Tablero", action = "Inicio", id = UrlParameter.Optional }
            //   );
            //}
            //else
            //{
                routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                //defaults: new { controller = "Tablero", action = "Inicio", id = UrlParameter.Optional }
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            //}



        }
    }
}
