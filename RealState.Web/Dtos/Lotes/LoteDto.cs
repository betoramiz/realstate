﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.Dtos.Lotes
{
    public class LoteDto
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public decimal Precio { get; set; }
    }
}