﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.Dtos.Cliente
{
    public class ClienteFiltroResult
    {
        public string Nombre { get; set; }
        public string Contrato { get; set; }
        public string Lote { get; set; }
        public string Parcela { get; set; }
        public string Adeudo { get; set; }
    }
}