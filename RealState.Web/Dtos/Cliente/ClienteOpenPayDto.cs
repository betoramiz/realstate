﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.Dtos.Cliente
{
    public class ClienteOpenPayDto
    {
        public string Id { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Email { get; set; }
    }
}