﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RealState.Web.Models;

namespace RealState.Web.Dtos.Cliente
{
    public class Cliente
    {
        public string Id { get; set; }
        public string NombreCompleto { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public List<Contrato> Contratos { get; set; }
    }
}