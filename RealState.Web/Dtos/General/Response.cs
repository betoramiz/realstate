﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.Dtos.General
{
    public class Response
    {
        public bool success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}