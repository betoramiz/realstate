﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.Dtos.Pagos
{
    public class PagoDetailDto
    {
        public int PagoId { get; set; }
        public string ContratoId { get; set; }
        public string ContratoNombre { get; set; }
        public string ClienteId { get; set; }
        public string ClienteNombre { get; set; }
        public DateTime Fecha { get; set; }
        public string Monto { get; set; }
        public string Mensualidad { get; set; }
        public string TotalMensualidades { get; set; }
    }
}