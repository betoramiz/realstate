﻿using System;

namespace RealState.Web.Dtos.Banco
{
    public class Banco
    {

        public string Id { get; set; }
        public string Description { get; set; }
        public string ErrorMessage { get; set; }
        public object Authorization { get; set; }
        public decimal Amount { get; set; }
        public string OperationType { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public string OrderId { get; set; }
        public DateTime CreationDate { get; set; }
        public string Status { get; set; }
        public string Method { get; set; }

    }
}