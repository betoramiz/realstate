﻿using System;

namespace RealState.Web.Dtos.Banco
{
    public class PaymentMethod
    {
        public string Type { get; set; }
        public string BankName { get; set; }
        public string Agreement { get; set; }
        public string CLABE { get; set; }
        public string Name { get; set; }
    }
}