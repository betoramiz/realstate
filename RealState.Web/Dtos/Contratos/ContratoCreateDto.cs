﻿using RealState.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealState.Web.Dtos.Contratos
{
    public class ContratoClienteDto
    {
        public string Id { get; set; }
        public string NombreCompleto { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
    }

    public class ContratoParcelaDto
    {
        public int id { get; set; }
        public string Numero { get; set; }
    }

    public class ContratoLoteDto
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public double Precio { get; set; }
        public int ParcelaId { get; set; }
    }

    public class ContratoCreateDto
    {
        public List<ContratoParcelaDto> Parcelas { get; set; }
        public List<ContratoClienteDto> Clientes { get; set; }
    }

    public class ContratoIndicadores
    {
        public int Morosos { get; set; }
        public int Puntuales { get; set; }
        public int Liquidaciones { get; set; }
    }
}