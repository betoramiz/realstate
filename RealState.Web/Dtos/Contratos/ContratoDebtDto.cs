﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.Dtos.Contratos
{
    public class ContratoDebtDto
    {
        public int PagoId { get; set; }
        public int ContratoId { get; set; }
        public DateTime FechaPago { get; set; }
        public string Mensualidad { get; set; }
        public decimal Monto { get; set; }
    }
}