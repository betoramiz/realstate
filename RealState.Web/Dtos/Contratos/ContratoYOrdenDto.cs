﻿using RealState.Web.Models;
using System.Collections.Generic;

namespace RealState.Web.Dtos.Contratos
{
    public class ContratoYOrdenDto
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string ClienteId { get; set; }
        public List<OrdenPago> Ordenes { get; set; }
    }
}