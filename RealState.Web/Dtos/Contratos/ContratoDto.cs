﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.Dtos.Contratos
{
    public class ContratoDto
    {
        public string Id { get; set; }
        public string Numero { get; set; }
        public string Fecha { get; set; }
        public bool Enable { get; set; }
        public bool Liquidado { get; set; }
        public string Cliente { get; set; }
        public string Lote { get; set; }
    }
}