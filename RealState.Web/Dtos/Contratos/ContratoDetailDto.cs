﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.Dtos.Contratos
{
	public class ContratoDetailDto
	{
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string ClienteId { get; set; }
        public string ClienteNombre { get; set; }
        public string  Enganche { get; set; }
        public int Parcialidades { get; set; }
        public string LoteNombre { get; set; }
        public string Precio { get; set; }
    }
}