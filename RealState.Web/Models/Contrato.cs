﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RealState.Web.Models
{
    [Table("Contrato")]
    public class Contrato
    {
        public Contrato()
        {
            Ordenes = new List<OrdenPago>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Numero { get; set; }

        public DateTime Fecha { get; set; }

        [Required]
        public int NumeroDeParcialidades { get; set; }

        [Required]
        public int Tipo { get; set; }
        public bool Enable { get; set; }
        public double Enganche { get; set; }

        public string ClientId { get; set; }
        public Cliente Cliente { get; set; }


        [ForeignKey("Lote")]
        public int LoteId { get; set; }
        public Lote Lote { get; set; }

        public List<OrdenPago> Ordenes { get; set; }


    }
}
