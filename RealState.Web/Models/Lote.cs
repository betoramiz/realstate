﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RealState.Web.Models
{
    [Table("Lote")]
    public class Lote
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Numero { get; set; }

        [Required]
        public double Largo { get; set; }

        [Required]
        public double Ancho { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }

        [Required]
        public decimal Precio { get; set; }
        public bool Enable { get; set; }

        [ForeignKey("Parcela")]
        public int ParcelaId { get; set; }
        public Parcela Parcela { get; set; }
    }
}
