﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RealState.Web.Models
{
    [Table("Pago")]
    public class Pago
    {
        [Key]
        public int Id { get; set; }
        public DateTime Fecha { get; set; }

        [Required]
        public string Mensualidad { get; set; }

        [Required]
        public decimal Monto { get; set; }

        [Required]
        public int FormaDePago { get; set; }
        public string CLABE { get; set; }

        public bool Pagado { get; set; }

        public string Referencia { get; set; }

        [ForeignKey("Contrato")]
        public int ContratoId { get; set; }
        public Contrato Contrato { get; set; }
    }
}
