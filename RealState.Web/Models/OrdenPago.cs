﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace RealState.Web.Models
{
    [Table("OdenPago")]
    public class OrdenPago
    {
        [Key]
        public int Id { get; set; }

        public string IdOpenPay { get; set; }


        public string Referencia { get; set; }

        public string ConvenioCIE { get; set; }

        public decimal Importe { get; set; }

        public string Concepto { get; set; }

        public DateTime FechaVencimiento { get; set; }

        public int ContratoId { get; set; }
    }
}