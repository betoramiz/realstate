﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RealState.Web.Models
{
    public class Login
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Contrasena { get; set; }
    }
}