﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RealState.Web.Models
{
    public class Cliente : IdentityUser
    {
        public Cliente()
        {
            Contratos = new List<Contrato>();
        }

        [Required]
        [MaxLength(512)]
        public string Nombres { get; set; }

        [Required]
        [MaxLength(512)]
        public string Apellidos { get; set; }

        [Required]
        [MaxLength(10)]
        public string Celular { get; set; }
        public int Tipo { get; set; }
        public string CLABE { get; set; }
        public bool Enable { get; set; }

        public ICollection<Contrato> Contratos { get; set; }
    }
}
