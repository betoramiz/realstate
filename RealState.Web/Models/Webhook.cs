﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RealState.Web.Models
{
    [Table("Webhook")]
    public class Webhook
    {
        [Key]
        public Guid Id { get; set; }
        public string type { get; set; }
        public string event_date { get; set; }
        public string verification_code { get; set; }
        public string transaction_amount { get; set; }
        public string transaction_authorization { get; set; }
        public string transaction_method { get; set; }
        public string transaction_operation_type { get; set; }
        public string transaction_transaction_type { get; set; }
        public string transaction_card_type { get; set; }
        public string transaction_card_brand { get; set; }
        public string transaction_card_address_line1 { get; set; }
        public string transaction_card_address_line2 { get; set; }
        public string transaction_card_address_line3 { get; set; }
        public string transaction_card_address_state { get; set; }
        public string transaction_card_address_city { get; set; }
        public string transaction_card_address_postal_code  { get; set; }
        public string transaction_card_address_country_code { get; set; }
        public string transaction_card_card_number { get; set; }
        public string transaction_card_holder_name { get; set; }
        public string transaction_card_expiration_month { get; set; }
        public string transaction_card_expiration_year  { get; set; }
        public string transaction_card_allows_charges   { get; set; }
        public string transaction_card_allows_payouts { get; set; }
        public string transaction_card_creation_date { get; set; }
        public string transaction_card_bank_name { get; set; }
        public string transaction_card_bank_code { get; set; }
        public string transaction_status { get; set; }
        public string transaction_id { get; set; }
        public string transaction_creation_date { get; set; }
        public string transaction_description   { get; set; }
        public string transaction_error_message { get; set; }
        public string transaction_order_id { get; set; }

        public Webhook(string type, string event_date, string verification_code, string transaction_amount, string transaction_authorization, string transaction_method, string transaction_operation_type, string transaction_transaction_type, string transaction_card_type, string transaction_card_brand, string transaction_card_address_line1, string transaction_card_address_line2, string transaction_card_address_line3, string transaction_card_address_state, string transaction_card_address_city, string transaction_card_address_postal_code, string transaction_card_address_country_code, string transaction_card_card_number, string transaction_card_holder_name, string transaction_card_expiration_month, string transaction_card_expiration_year, string transaction_card_allows_charges, string transaction_card_allows_payouts, string transaction_card_creation_date, string transaction_card_bank_name, string transaction_card_bank_code, string transaction_status, string transaction_id, string transaction_creation_date, string transaction_description, string transaction_error_message, string transaction_order_id)
        {
            this.type = type;
            this.event_date = event_date;
            this.verification_code = verification_code;
            this.transaction_amount = transaction_amount;
            this.transaction_authorization = transaction_authorization;
            this.transaction_method = transaction_method;
            this.transaction_operation_type = transaction_operation_type;
            this.transaction_transaction_type = transaction_transaction_type;
            this.transaction_card_type = transaction_card_type;
            this.transaction_card_brand = transaction_card_brand;
            this.transaction_card_address_line1 = transaction_card_address_line1;
            this.transaction_card_address_line2 = transaction_card_address_line2;
            this.transaction_card_address_line3 = transaction_card_address_line3;
            this.transaction_card_address_state = transaction_card_address_state;
            this.transaction_card_address_city = transaction_card_address_city;
            this.transaction_card_address_postal_code = transaction_card_address_postal_code;
            this.transaction_card_address_country_code = transaction_card_address_country_code;
            this.transaction_card_card_number = transaction_card_card_number;
            this.transaction_card_holder_name = transaction_card_holder_name;
            this.transaction_card_expiration_month = transaction_card_expiration_month;
            this.transaction_card_expiration_year = transaction_card_expiration_year;
            this.transaction_card_allows_charges = transaction_card_allows_charges;
            this.transaction_card_allows_payouts = transaction_card_allows_payouts;
            this.transaction_card_creation_date = transaction_card_creation_date;
            this.transaction_card_bank_name = transaction_card_bank_name;
            this.transaction_card_bank_code = transaction_card_bank_code;
            this.transaction_status = transaction_status;
            this.transaction_id = transaction_id;
            this.transaction_creation_date = transaction_creation_date;
            this.transaction_description = transaction_description;
            this.transaction_error_message = transaction_error_message;
            this.transaction_order_id = transaction_order_id;
        }
    }
}