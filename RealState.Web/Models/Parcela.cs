﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RealState.Web.Models
{
    [Table("Parcela")]
    public class Parcela
    {
        public Parcela()
        {
            Lotes = new List<Lote>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Numero { get; set; }

        [Required]
        public string Largo { get; set; }

        [Required]
        public string Ancho { get; set; }
        public bool Enable { get; set; }

        public ICollection<Lote> Lotes { get; set; }
    }
}
