﻿using RealState.Web.Models;
using System.Threading.Tasks;

namespace RealState.Web.Services
{
    public interface IOrdenPagoService
    {
        Task GuardaOrden(ViewModels.OrdenPago.OrdenPagoViewModel orden, int contratoId);
        Task<OrdenPago> ObtenOrdenPagoPorContrato(int contratoId);
    }
}
