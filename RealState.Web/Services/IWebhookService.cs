﻿using RealState.Web.ViewModels.Contratos;
using System.Threading.Tasks;

namespace RealState.Web.Services
{
    public interface IWebhookService
    {
        void AddWebhookAsync(Webhook contrato, ref int Contrato_Id, ref string Email, ref string Telefono, ref string Nombre);
    }
}