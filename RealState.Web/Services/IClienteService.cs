﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealState.Web.ViewModels.Clientes;

namespace RealState.Web.Services
{
    public interface IClienteService
    {
        Task<List<ViewModels.Clientes.ClienteVM>> ListaClientesAsync();
        Task<List<ViewModels.Clientes.ClienteVM>> ListaClientesWithContactAsync();
        Task CreateAsync(ViewModels.Clientes.ClienteCreate cliente);
        Task<List<ViewModels.Clientes.ClienteVM>> GetClientByPhone(string phone);
        Task<List<ViewModels.Clientes.ClienteVM>> GetClientWithContract(string phone);
        Task<ViewModels.Clientes.ClienteVM> GetClienteById(string id);
        Task<Dtos.Cliente.ClienteOpenPayDto> GetClienteByIdOpenPay(string id);
        Task<ViewModels.Clientes.ClienteUpdate> GetClienteByIdForUpdate(string id);
        Task UpdateAsync(ClienteUpdate cliente);
    }
}
