﻿using RealState.Web.ViewModels.Contratos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RealState.Web.Services
{
    public interface IContratoService
    {
        Task<List<Dtos.Contratos.ContratoDto>> ListaAsync();
        ContratoCreate GetContratoCreateItems();
        Task AddContractAsync(ContratoCreate contrato);
        Task<Dtos.Contratos.ContratoDto> GetContratoByName(string name);
        Task<Dtos.Contratos.ContratoDetailDto> GetContratoById(int id);
        Task<Dtos.Contratos.ContratoYOrdenDto> GetContratoAndordenes(int contratoId);
        Task<Dtos.Contratos.ContratoDebtDto> GetPayAmount(int ContratoId);
        Task<Dtos.Contratos.ContratoIndicadores> GetIndicadores();
    }
}
