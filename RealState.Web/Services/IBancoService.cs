﻿namespace RealState.Web.Services
{
    public interface IBancoService
    {
        Dtos.Banco.Banco GeneraPago(decimal monto, Dtos.Cliente.ClienteOpenPayDto cliente, string ordenId);
    }
}
