﻿using RealState.Web.ViewModels.Lotes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RealState.Web.Services
{
    public interface ILoteService
    {
        Task<List<Lotes>> BuscarPorNumeroAsync(string numero);
        Task<Lotes> GetByIdAsync(int id);
        Task UpdateAsync(LoteCreate lote);
        Task CreateAsync(LoteCreate lote);
        Task<List<Lotes>> GetFirst50Async();
        Task<List<Lotes>> GetListOfEnablesAsync();
        Task<List<Dtos.Lotes.LoteDto>> GetForParcelas(int parcelaId);
    }
}