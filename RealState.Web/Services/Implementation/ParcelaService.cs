﻿using RealState.Web.Infrastructure.Repositories;
using vm = RealState.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using RealState.Web.Models;
using RealState.Web.ViewModels.Parcelas;

namespace RealState.Web.Services.Implementation
{
    public class ParcelaService : IParcelaService
    {
        private IParcelaRepository parcelaRepo;
        private IMapper mapper;
        public ParcelaService(IParcelaRepository parcelaRepo, IMapper mapper)
        {
            this.parcelaRepo = parcelaRepo;
            this.mapper = mapper;
        }

        public async Task CreateAsync(ParcelaCreate parcela)
        {
            var parcelaExisiting = await parcelaRepo.GetByNumberAsync(parcela.Numero);

            if (parcelaExisiting.Count > 0)
                throw new System.Exception($"La parcela con el numero {parcela.Numero} ya existe");

            var parcelaMapped = mapper.Map<ParcelaCreate, Parcela>(parcela);
            await parcelaRepo.Create(parcelaMapped);
        }

        public async Task<vm.Parcelas.Parcelas> FindByIdAsync(int id)
        {
            var parcela = await parcelaRepo.GetByIdAsyncAndIncludeLotes(id);
            var parcelaVm = mapper.Map<Parcela, vm.Parcelas.Parcelas>(parcela);

            return parcelaVm;
        }

        public async Task<List<vm.Parcelas.Parcelas>> GetListAsync()
        {
            var parcelas =  await parcelaRepo.GetListAsync();
            var parcelasVm = mapper.Map<IEnumerable<Parcela>, List<vm.Parcelas.Parcelas>>(parcelas);

            return parcelasVm;
        }

        public async Task<List<Models.Parcela>> GetListParcelasAsync()
        {
            return await parcelaRepo.GetListAsync();
        }
    }
}