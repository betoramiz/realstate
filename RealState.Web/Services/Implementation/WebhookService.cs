﻿using AutoMapper;
using RealState.Web.Infrastructure.Repositories;
using System;
using System.Threading.Tasks;

namespace RealState.Web.Services.Implementation
{
    public class WebhookService : IWebhookService
    {

        private ContratoRepository contratoRepo;
        private IMapper mapper;

        public void AddWebhookAsync(ViewModels.Contratos.Webhook webhook, ref int Contrato_Id, ref string Email, ref string Telefono, ref string Nombre)
        {
            try
            {
                Models.Webhook webhookModel = new Models.Webhook(
                    webhook.type,
                    webhook.event_date,
                    webhook.verification_code,
                    webhook.transaction.amount.ToString(),
                    webhook.transaction.authorization,
                    webhook.transaction.method,
                    webhook.transaction.operation_type,
                    webhook.transaction.transaction_type,
                    webhook.transaction.card.type,
                    webhook.transaction.card.brand,
                    webhook.transaction.card.address.line1,
                    webhook.transaction.card.address.line2,
                    webhook.transaction.card.address.line3,
                    webhook.transaction.card.address.state,
                    webhook.transaction.card.address.city,
                    webhook.transaction.card.address.postal_code,
                    webhook.transaction.card.address.country_code,
                    webhook.transaction.card.card_number,
                    webhook.transaction.card.holder_name,
                    webhook.transaction.card.expiration_month,
                    webhook.transaction.card.expiration_year,
                    webhook.transaction.card.allows_charges.ToString(),
                    webhook.transaction.card.allows_payouts.ToString(),
                    webhook.transaction.card.creation_date,
                    webhook.transaction.card.bank_name,
                    webhook.transaction.card.bank_code,
                    webhook.transaction.status,
                    webhook.transaction.id,
                    webhook.transaction.creation_date,
                    webhook.transaction.description,
                    webhook.transaction.error_message,
                    webhook.transaction.order_id
                    );
                contratoRepo = new ContratoRepository(new Infrastructure.Context());
                contratoRepo.CreateWebhook(webhookModel, ref Contrato_Id, ref Email, ref Telefono, ref Nombre);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}