﻿using AutoMapper;
using Openpay;
using Openpay.Entities;
using Openpay.Entities.Request;
using RealState.Web.Models;
using System;
using System.Configuration;
using BancoDto = RealState.Web.Dtos.Banco;

namespace RealState.Web.Services.Implementation
{
    public class BancoService : IBancoService
    {
        private IMapper mapper;
        public BancoService(IMapper mapper)
        {
            this.mapper = mapper;
        }

        public BancoDto.Banco GeneraPago(decimal monto, Dtos.Cliente.ClienteOpenPayDto cliente, string ordenId)
        {
            var openPayId = ConfigurationManager.AppSettings.Get("OpenPayApiId");
            var openPaySecretKey = ConfigurationManager.AppSettings.Get("OpenPayPrivateKey");

            OpenpayAPI api = new OpenpayAPI(openPaySecretKey, openPayId);
            ChargeRequest request = new ChargeRequest();
            request.Method = "bank_account";
            request.Amount = monto;
            request.Description = "Cargo con banco";
            request.OrderId = $"oid-{ordenId}";
            request.Customer = new Customer()
            {
                Id = cliente.Id,
                LastName = cliente.Apellidos,
                Name = cliente.Nombres,
                Email = cliente.Email
                
            };
            var charge = new Charge();

            try
            {
                charge = api.ChargeService.Create(request);
                var chargeMapped = mapper.Map<Charge, BancoDto.Banco>(charge);
                return chargeMapped;
            }
            catch (Exception ex)
            {
                return new BancoDto.Banco();
            }
        }
    }
}