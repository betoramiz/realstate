﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RealState.Web.Dtos.Cliente;
using RealState.Web.Helper;
using RealState.Web.Infrastructure.Repositories;

namespace RealState.Web.Services.Implementation
{
    public class TableroService : ITableroService
    {
        private IClienteRepository clienteRepo;

        public TableroService(IClienteRepository clienteRepo)
        {
            this.clienteRepo = clienteRepo;
        }

        public List<ClienteFiltroResult> ObtenerClientePorFiltro(string filtro)
        { 
            return clienteRepo.ObtenerClientesPorFiltro(filtro);
        }
    }
}