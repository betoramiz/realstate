﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using RealState.Web.Dtos.Cliente;
using RealState.Web.Infrastructure.Repositories;
using RealState.Web.ViewModels.Clientes;

namespace RealState.Web.Services.Implementation
{
    public class ClienteService : IClienteService
    {
        private IClienteRepository clienteRepo;
        private IMapper mapper;
        public ClienteService(IClienteRepository clienteRepo, IMapper mapper)
        {
            this.clienteRepo = clienteRepo;
            this.mapper = mapper;
        }

        public async Task CreateAsync(ClienteCreate cliente)
        {
            var clienteMapped = mapper.Map<ClienteCreate, Models.Cliente>(cliente);
            await clienteRepo.Create(clienteMapped);
        }

        public async Task<List<ClienteVM>> GetClientByPhone(string phone)
        {
            var cliente = await clienteRepo.GetClientByphoneAsync(phone);
            var clienteMapped = mapper.Map<IEnumerable<Models.Cliente>, List<ViewModels.Clientes.ClienteVM>>(cliente);
            return clienteMapped;
        }

        public async Task<ClienteVM> GetClienteById(string id)
        {
            var cliente = await clienteRepo.GetByIdAsync(id);
            var clienteMapped = mapper.Map<Models.Cliente, ClienteVM>(cliente);
            return clienteMapped;
        }

        public async Task<ClienteUpdate> GetClienteByIdForUpdate(string id)
        {
            var cliente = await clienteRepo.GetByIdAsync(id);
            var clienteMapped = mapper.Map<Models.Cliente, ClienteUpdate>(cliente);
            return clienteMapped;
        }

        public async Task<ClienteOpenPayDto> GetClienteByIdOpenPay(string id)
        {
            var cliente = await clienteRepo.GetByIdAsync(id);
            var clienteMapped = mapper.Map<Models.Cliente, ClienteOpenPayDto>(cliente);
            return clienteMapped;
        }

        public async Task<List<ClienteVM>> GetClientWithContract(string phone)
        {
            var cliente = await clienteRepo.GetClientByphoneAsync(phone);
            var clientsWithContract = cliente.Where(contracto => contracto.Contratos.Count > 0);
            var clienteMapped = mapper.Map<IEnumerable<Models.Cliente>, List<ViewModels.Clientes.ClienteVM>>(clientsWithContract);
            return clienteMapped;
        }

        public async Task<List<ClienteVM>> ListaClientesAsync()
        {
            var clientes = await clienteRepo.GetTop50Async();
            var clientesVm = mapper.Map<IEnumerable<Models.Cliente>, List<ClienteVM>>(clientes);
            return clientesVm;
        }

        public async Task<List<ClienteVM>> ListaClientesWithContactAsync()
        {
            var clientes = await clienteRepo.GetTop50Async();
            var clientesWithcontract = clientes.Where(cliente => cliente.Contratos.Count > 0 && cliente.Enable == true);
            var clientesVm = mapper.Map<IEnumerable<Models.Cliente>, List<ClienteVM>>(clientesWithcontract);
            return clientesVm;
        }

        public async Task UpdateAsync(ClienteUpdate cliente)
        {
            var clienteMapped = mapper.Map<ClienteUpdate, Models.Cliente>(cliente);
            await clienteRepo.Update(clienteMapped);
        }
    }
}