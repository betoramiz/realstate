﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using RealState.Web.Dtos.Contratos;
using RealState.Web.Infrastructure.Repositories;
using RealState.Web.ViewModels.Contratos;

namespace RealState.Web.Services.Implementation
{
    public class ContratoService : IContratoService
    {
        private IContratoRepository contratoRepo;
        private IMapper mapper;

        public ContratoService(IContratoRepository contratoRepo, IMapper mapper)
        {
            this.contratoRepo = contratoRepo;
            this.mapper = mapper;
        }

        public async Task AddContractAsync(ContratoCreate contrato)
        {
            try
            {
                var exisiteContrato = await contratoRepo.ExisitContract(contrato.Nombre);

                if (exisiteContrato)
                    throw new Exception($"El contrato con el nombre {contrato.Nombre} ya esta dado de alta, intente con otro nombre");

                var contratoModel = mapper.Map<ContratoCreate, Models.Contrato>(contrato);
                contratoRepo.CreateByProcedure(contratoModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public async Task<ContratoYOrdenDto> GetContratoAndordenes(int contratoId)
        {
            var contratoDto = new ContratoYOrdenDto();
            try
            {
                var contrato = await contratoRepo.GetContrato(contratoId);
                contratoDto = mapper.Map<Models.Contrato, ContratoYOrdenDto>(contrato);

                return contratoDto;
            }
            catch (Exception ex)
            {
                return contratoDto;
            }
        }

        public async Task<ContratoDetailDto> GetContratoById(int id)
        {
            var contratoDto = new ContratoDetailDto();
            try
            {
                var contrato = await contratoRepo.GetContrato(id);
                contratoDto = mapper.Map<Models.Contrato, ContratoDetailDto>(contrato);

                return contratoDto;
            }
            catch (Exception ex)
            {
                return contratoDto;
            }
            
        }

        public async Task<ContratoDto> GetContratoByName(string name)
        {
            var contrato = await contratoRepo.GetByNameAsync(name);
            var contratoDto = mapper.Map<Models.Contrato, Dtos.Contratos.ContratoDto>(contrato);
            return contratoDto;
        }

        public ViewModels.Contratos.ContratoCreate GetContratoCreateItems()
        {
            var contratoItems = contratoRepo.GetContratoCreteItems();
            var contratoMapped = mapper.Map<Dtos.Contratos.ContratoCreateDto, ViewModels.Contratos.ContratoCreate>(contratoItems);
            return contratoMapped;
        }

        public async Task<ContratoDebtDto> GetPayAmount(int ContratoId)
        {
            var adeudos = await contratoRepo.GetAdeudos(ContratoId);
            var contratoDeb = new ContratoDebtDto();
            if(adeudos.Count > 0)
            {
                contratoDeb = adeudos.First();
                contratoDeb.Monto = adeudos.Sum(x => x.Monto);
            }

            return contratoDeb;
        }

        public async Task<List<ContratoDto>> ListaAsync()
        {
            var contracts = await contratoRepo.Get50FirstAsync();
            var contractsMapped = mapper.Map<IEnumerable<Models.Contrato>, List<Dtos.Contratos.ContratoDto>>(contracts);
            return contractsMapped;
        }

        public async Task<Dtos.Contratos.ContratoIndicadores> GetIndicadores()
        {
            var adeudos = await contratoRepo.GetIndicadores();
            return adeudos;
        }
    }
}