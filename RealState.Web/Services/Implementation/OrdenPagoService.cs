﻿using AutoMapper;
using RealState.Web.Infrastructure.Repositories;
using RealState.Web.Models;
using RealState.Web.ViewModels.OrdenPago;
using System;
using System.Threading.Tasks;

namespace RealState.Web.Services.Implementation
{
    public class OrdenPagoService : IOrdenPagoService
    {
        private IOrdenPagoRepository ordenPagoRepo;
        private IContratoRepository contratoRepository;
        private IMapper mapper;

        public OrdenPagoService(IOrdenPagoRepository ordenPagoRepo, IContratoRepository contratoRepository, IMapper mapper)
        {
            this.ordenPagoRepo = ordenPagoRepo;
            this.contratoRepository = contratoRepository;
            this.mapper = mapper;
        }

        public async Task GuardaOrden(OrdenPagoViewModel orden, int contratoId)
        {
            try
            {
                var ordenPago = mapper.Map<OrdenPagoViewModel, OrdenPago>(orden);
                var contrato = await contratoRepository.GetByIdAsync(contratoId);
                ordenPago.ContratoId = contrato.Id;
                await ordenPagoRepo.Create(ordenPago);
            }
            catch (Exception ex)
            {
                //save to log
                throw;
            }
        }

        public async Task<OrdenPago> ObtenOrdenPagoPorContrato(int contratoId)
        {
            var ordenpago = await ordenPagoRepo.OrdenPagoPorcontrato(contratoId);
            return ordenpago;
        }
    }
}