﻿using AutoMapper;
using RealState.Web.Infrastructure.Repositories;
using vm = RealState.Web.ViewModels;
using RealState.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using RealState.Web.ViewModels.Lotes;
using RealState.Web.Dtos.Lotes;

namespace RealState.Web.Services.Implementation
{
    public class LoteService : ILoteService
    {
        private ILoteRepository loteRepo;
        private IMapper mapper;
        public LoteService(ILoteRepository loteRepo, IMapper mapper)
        {
            this.loteRepo = loteRepo;
            this.mapper = mapper;
        }

        public async Task<List<vm.Lotes.Lotes>> BuscarPorNumeroAsync(string numero)
        {
            if (string.IsNullOrEmpty(numero) || string.IsNullOrWhiteSpace(numero))
                return new List<vm.Lotes.Lotes>();

            var lotes = await loteRepo.GetByNumberAsync(numero);
            var lotesVm = mapper.Map<IEnumerable<Lote>, List<vm.Lotes.Lotes>>(lotes);

            return lotesVm;
        }

        public async Task CreateAsync(LoteCreate lote)
        {
            try
            {
                var exitingLote = await loteRepo.GetByNumberAsync(lote.Numero);
                if (exitingLote.Count >= 1)
                    throw new Exception($"Ya existe un lote con el Numero {lote.Numero}");

                var loteMapped = mapper.Map<LoteCreate, Models.Lote>(lote);
                await loteRepo.Create(loteMapped);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<vm.Lotes.Lotes>> GetFirst50Async()
        {
            var lotes = await loteRepo.GetTop50LotesAsync();
            var lotesVm = mapper.Map<IEnumerable<Models.Lote>, List<vm.Lotes.Lotes>> (lotes);

            return lotesVm;
        }

        public async Task<vm.Lotes.Lotes> GetByIdAsync(int id)
        {
            var lote = await loteRepo.GetByIdAsyncAndParcela(id);

            if (lote == null)
                throw new Exception($"Lote con Id {id} no encontrado");

            var lotevm = mapper.Map<Models.Lote, vm.Lotes.Lotes>(lote);
            return lotevm;
        }

        public async Task UpdateAsync(LoteCreate lote)
        {
            var loteExisting = await loteRepo.GetByNumberAsync(lote.Numero);
            if (loteExisting.Count == 0)
                throw new Exception($"El lote con numero {lote.Numero} no exisite");
            else if(loteExisting.Count > 1)
                throw new Exception($"Exisite mas de un lote con el numero {lote.Numero}");

            var loteModel = mapper.Map<LoteCreate, Lote>(lote);
            await loteRepo.Update(loteModel);
        }

        public async Task<List<Lotes>> GetListOfEnablesAsync()
        {
            var lotes = await loteRepo.GetListOfEnablesAsync();
            var lotesvm = mapper.Map<IEnumerable<Lote>, List<Lotes>>(lotes);
            return lotesvm;
        }

        public async Task<List<LoteDto>> GetForParcelas(int parcelaId)
        {
            var lotes = await loteRepo.GetForParcela(parcelaId);
            var lotesDto = mapper.Map<IEnumerable<Lote>, List<Dtos.Lotes.LoteDto>>(lotes);
            return lotesDto;
        }
    }
}   