﻿using AutoMapper;
using RealState.Web.Enums;
using RealState.Web.Infrastructure.Repositories;
using RealState.Web.Models;
using RealState.Web.ViewModels.Pagos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RealState.Web.Services.Implementation
{
    public class PagoService : IPagoService
    {
        private IPagoRepository pagoRepository;
        private IMapper mapper;
        public PagoService(IPagoRepository pagoRepository, IMapper mapper)
        {
            this.pagoRepository = pagoRepository;
            this.mapper = mapper;
        }

        public void AddPago(int contratoId, string environment)
        {
            try
            {
                pagoRepository.AddPago(contratoId, environment);
            }
            catch (Exception ex)
            {
                //save to log ex
            }
        }

        public async Task<Models.Pago> AddPayments(int pagoId)
        {
            var pagoCorriente = new Models.Pago() { Pagado = false};
            try
            {
                pagoCorriente = await pagoRepository.GetByIdAsync(pagoId);
                if (pagoCorriente == null)
                    throw new Exception("El recurso no existe");

                pagoCorriente.Pagado = true;
                await pagoRepository.Update(pagoCorriente);

                return pagoCorriente;
            }
            catch (Exception ex)
            {
                //save to log ex
                return pagoCorriente;
            }
        }

        public async Task<List<AddPagoVM>> GetDetailByCobtratoId(int id)
        {
            var pagos = await pagoRepository.GetPagosByContractId(id);
            var pagosVm = mapper.Map<List<Pago>, List<AddPagoVM>>(pagos);

            return pagosVm;
        }

        public async Task<AddPagoVM> GetDetailByIdAsync(int id = 0)
        {
            var pago = await pagoRepository.GetDetailByIdAsync(id);
            if (pago == null)
                return new AddPagoVM() { Id = 0, Monto = 0, Nombre = "" };

            var pagoMapperd = mapper.Map<Pago, AddPagoVM>(pago);

            return pagoMapperd;
        }

        public async Task<List<PagosVm>> GetPagosByContratoId(int contratoId)
        {
            var pagos = await  pagoRepository.GetPagosVencidosByContractId(contratoId);
            var pagosMapped =  mapper.Map<List<Pago>, List<PagosVm>>(pagos);

            return pagosMapped;
        }

        public async Task<List<PagosVm>> PagosCurrentMonth()
        {
            var pagosInMonth = await pagoRepository.GetPayListCurrentMonth();
            return PagoList(pagosInMonth);
        }

        public async Task<List<PagosVm>> PagosinMonth(Months month)
        {
            var pagosInMonth = await pagoRepository.GetPayListInMont(month);
            return PagoList(pagosInMonth);
        }

        private List<PagosVm> PagoList(List<Models.Pago> pagos)
        {
            var pagosVm = mapper.Map<IEnumerable<Models.Pago>, List<PagosVm>>(pagos);
            return pagosVm;
        }
    }
}