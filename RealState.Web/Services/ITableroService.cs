﻿using RealState.Web.Dtos.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealState.Web.Services
{
    public interface ITableroService
    {
        List<ClienteFiltroResult> ObtenerClientePorFiltro(string filtro);
    }
}
