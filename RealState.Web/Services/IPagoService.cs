﻿using RealState.Web.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealState.Web.Services
{
    public interface IPagoService
    {
        Task<Models.Pago> AddPayments(int PagoId);
        void AddPago(int contratoId, string environment);
        Task<List<ViewModels.Pagos.PagosVm>> PagosCurrentMonth();
        Task<List<ViewModels.Pagos.PagosVm>> PagosinMonth(Months month);
        Task<ViewModels.Pagos.AddPagoVM> GetDetailByIdAsync(int id);
        Task<List<ViewModels.Pagos.AddPagoVM>> GetDetailByCobtratoId(int id);
        Task<List<ViewModels.Pagos.PagosVm>> GetPagosByContratoId(int contratoId);
    }
}
