﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vm = RealState.Web.ViewModels.Parcelas;
using System.Web;

namespace RealState.Web.Services
{
    public interface IParcelaService
    {
        Task<List<vm.Parcelas>> GetListAsync();
        Task CreateAsync(vm.ParcelaCreate parcela);
        Task<vm.Parcelas> FindByIdAsync(int id);
        Task<List<Models.Parcela>> GetListParcelasAsync();
    }
}