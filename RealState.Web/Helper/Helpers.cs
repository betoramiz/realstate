﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.Helper
{
    public class Helpers
    {
        public static bool IsValid(string stringDate)
        {
            DateTime validDate;
            return DateTime.TryParse(stringDate, out validDate);
        }

        public static DateTime StringToDateTime(string stringDate)
        {
            DateTime dateTime = DateTime.Parse(stringDate);
            return dateTime;
        }
    }
}