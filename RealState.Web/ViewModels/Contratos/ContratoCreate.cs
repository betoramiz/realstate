﻿using RealState.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealState.Web.ViewModels.Contratos
{
    public class ContratoCreate
    {

        public ContratoCreate()
        {
            Parcelas = new List<Dtos.Contratos.ContratoParcelaDto>();
            Lotes = new List<Dtos.Contratos.ContratoLoteDto>();
            Clientes = new List<Dtos.Contratos.ContratoClienteDto>();
        }

        [Required(ErrorMessage = "Este campo es requerido")]
        [Display(Name = "Numero de Contrato")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [Display(Name = "Parcela")]
        public int ParcelaId { get; set; }
        public IEnumerable<Dtos.Contratos.ContratoParcelaDto> Parcelas { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [Display(Name = "Lote")]
        public int LoteId { get; set; }
        public IEnumerable<Dtos.Contratos.ContratoLoteDto> Lotes { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [Display(Name = "Cliente")]
        public string ClienteId { get; set; }
        public IEnumerable<Dtos.Contratos.ContratoClienteDto> Clientes { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [NumericValidator(ErrorMessage = "Este campo solo admite numeros")]
        public string Enganche { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [NumericValidator(ErrorMessage = "Este campo solo admite numeros")]
        public int Mensualidades { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [DataType(DataType.Date, ErrorMessage = "Este campo solo admite fechas validas")]
        //[DateMinValidator(ErrorMessage = "La Fecha no puede ser menor al dia actual")]
        [Display(Name = "Inicio de Contrato")]
        public string FechaPrimerPago { get; set; }
    }
}