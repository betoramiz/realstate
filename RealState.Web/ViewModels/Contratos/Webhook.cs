﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.ViewModels.Contratos
{
    public class Webhook
    {
        public const string VERIFICATION = "verification";
        public const string CHARGE_CREATED = "charge.created";
        public const string CHARGE_SUCCEEDED = "charge.succeeded";
        public const string CHARGE_REFUNDED = "charge.refunded";
        public const string PAYOUT_CREATED = "payout.created";
        public const string PAYOUT_SUCCEEDED = "payout.succeeded";
        public const string PAYOUT_FAILED = "payout.failed";
        public const string TRANSFER_SUCCEEDED = "transfer.succeeded";
        public const string FEE_SUCCEEDED = "fee.succeeded";
        public const string SPEI_RECEIVED = "spei.received";
        public const string CHARGEBACK_CREATED = "chargeback.created";
        public const string CHARGEBACK_REJECTED = "chargeback.rejected";
        public const string CHARGEBACK_ACCEPTED = "chargeback.accepted";

        public string type { get; set; }
        public string event_date { get; set; }
        public string verification_code { get; set; }
        public Transaccion transaction { get; set; }
    }

    public class Transaccion
    {
        public double amount { get; set; }
        public string authorization { get; set; }
        public string method { get; set; }
        public string operation_type { get; set; }
        public string transaction_type { get; set; }
        public Card card { get; set; }
        public string status { get; set; }
        public string id { get; set; }
        public string creation_date { get; set; }
        public string description { get; set; }
        public string error_message { get; set; }
        public string order_id { get; set; }
    }

    public class Card
    {
        public string type { get; set; }
        public string brand { get; set; }
        public Address address { get; set; }
        public string card_number { get; set; }
        public string holder_name { get; set; }
        public string expiration_month { get; set; }
        public string expiration_year { get; set; }
        public bool allows_charges { get; set; }
        public bool allows_payouts { get; set; }
        public string creation_date { get; set; }
        public string bank_name { get; set; }
        public string bank_code { get; set; }
    }

    public class Address
    {
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string line3 { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string postal_code { get; set; }
        public string country_code { get; set; }
    }
}