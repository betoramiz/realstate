﻿using RealState.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.ViewModels.Clientes
{
    public class ClienteVM
    {
        public string Id { get; set; }
        public string NombreCompleto { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public int Contratos { get; set; }
        public List<Contrato> ListaContratos { get; set; }
    }
}