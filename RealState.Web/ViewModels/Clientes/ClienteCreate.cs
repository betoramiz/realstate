﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealState.Web.ViewModels.Clientes
{
    public class ClienteCreate
    {
        [Required(ErrorMessage = "Este campo es requerido")]
        [MaxLength(512, ErrorMessage = "El numero maximo de caracteres es de 512")]
        public string Nombres { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [MaxLength(512, ErrorMessage = "El numero maximo de caracteres es de 512")]
        public string Apellidos { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [MaxLength(10, ErrorMessage = "El formato de celular es de 10 digitos")]
        [Phone(ErrorMessage = "El celular el celular no tiene el formato correcto")]
        public string Celular { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [EmailAddress(ErrorMessage = "el Correo electronico no tiene el formato correcto")]
        [MaxLength(512, ErrorMessage = "El numero maximo de caracteres es de 512")]
        public string Email { get; set; }
    }
}