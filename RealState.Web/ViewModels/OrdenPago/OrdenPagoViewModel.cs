﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.ViewModels.OrdenPago
{
    public class OrdenPagoViewModel
    {
        public string IdOpenPay { get; set; }

        public string Referencia { get; set; }

        public string ConvenioCIE { get; set; }

        public decimal Importe { get; set; }

        public string Concepto { get; set; }

        public DateTime FechaVencimiento { get; set; }
    }
}