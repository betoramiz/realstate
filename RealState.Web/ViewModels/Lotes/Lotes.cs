﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealState.Web.ViewModels.Lotes
{
    public class Lotes
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Medidas { get; set; }
        public string Ubicacion { get; set; }
        public decimal Precio { get; set; }
        [Display(Name = "Activo")]
        public bool Enable { get; set; }
        public string Parcela { get; set; }

    }
}