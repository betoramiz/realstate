﻿using DataAnnotationsExtensions;
using RealState.Web.Attributes;
using RealState.Web.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RealState.Web.ViewModels.Lotes
{
    public class LoteCreate
    {
        public LoteCreate()
        {
            Parcelas = new List<Parcela>();
        }

        [Required(ErrorMessage = "Este campo es requerido")]
        public string Numero { get; set; }

        [MinValidator(MinValue =1, ErrorMessage = "El Valor minimo para el Largo es de 1")]
        [NumericValidator(ErrorMessage = "Este campo solo acepta numeros")]
        [Required(ErrorMessage = "Este campo es requerido")]
        public double Largo { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [MinValidator(MinValue = 1, ErrorMessage = "El Valor minimo para el Largo es de 1")]
        [NumericValidator(ErrorMessage = "Este campo solo acepta numeros")]
        public double Ancho { get; set; }

        public string Latitud { get; set; }
        public string Longitud { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [MinValidator(MinValue = 1, ErrorMessage = "El Valor minimo para el Largo es de 1")]
        [NumericValidator(ErrorMessage = "Este campo solo acepta numeros")]
        public decimal Precio { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [Display(Name = "Parcela")]
        public int ParcelaId { get; set; }
        public IEnumerable<Parcela> Parcelas { get; set; }
    }
}