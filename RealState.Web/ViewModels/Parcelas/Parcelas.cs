﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.ViewModels.Parcelas
{
    public class Parcelas
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Medidas { get; set; }
        public int Lotes { get; set; }
    }
}