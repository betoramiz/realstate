﻿using DataAnnotationsExtensions;
using RealState.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealState.Web.ViewModels.Parcelas
{
    public class ParcelaCreate
    {
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Numero { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [NumericValidator(ErrorMessage = "Este campo solo acepta numeros")]
        public string Largo { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        [NumericValidator(ErrorMessage = "Este campo solo acepta numeros")]
        public string Ancho { get; set; }
    }
}