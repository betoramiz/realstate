﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealState.Web.ViewModels.Pagos
{
    public class AddPagoVM
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public decimal Monto { get; set; }
        public bool Estatus { get; set; }
    }
}