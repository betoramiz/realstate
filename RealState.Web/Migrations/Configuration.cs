namespace RealState.Web.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RealState.Web.Infrastructure.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RealState.Web.Infrastructure.Context context)
        {
            var cliente = new Models.Cliente
            {
                UserName = "gonzalo@email.com.mx",
                Email = "greyes10.9@gmail.com",
                Nombres = "Gonazlo",
                Apellidos = "Reyes",
                Celular = "87654321",
                Tipo = 0,
                EmailConfirmed = true,
                Enable = true,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            if (!context.Users.Any(u => u.Email == cliente.Email))
            {
                var userManager = new UserManager<Models.Cliente>(new UserStore<Models.Cliente>(context));


                var passwordHashed = userManager.PasswordHasher.HashPassword("admin123");
                cliente.PasswordHash = passwordHashed;

                userManager.Create(cliente);

            }

            var parcela = CreateParcela();
            context.Parcela.Add(parcela);

            var lotes = CreateLote(parcela);
            context.Lote.AddRange(lotes);

            context.SaveChanges();
        }

        private static Models.Parcela CreateParcela()
        {
            var lotes = new List<Models.Lote>();

            var parcela = new Models.Parcela()
            {
                Numero = "Parcela 1",
                Ancho = "100",
                Largo = "100",
                Enable = true,
                Lotes = lotes
            };

            return parcela;
        }

        public static List<Models.Lote> CreateLote(Models.Parcela parcela)
        {
            var lotes = new List<Models.Lote>();

            var lote1 = new Models.Lote()
            {
                Numero = "Lote 1",
                Precio = 100,
                Ancho = 100,
                Largo = 100,
                Latitud = null,
                Longitud = null,
                Enable = true,
                Parcela = parcela
            };

            var lote2 = new Models.Lote()
            {
                Numero = "Lote 2",
                Precio = 100,
                Ancho = 100,
                Largo = 100,
                Latitud = null,
                Longitud = null,
                Enable = true,
                Parcela = parcela
            };

            lotes.Add(lote1);
            lotes.Add(lote2);

            return lotes;
        }
    }
}
