namespace RealState.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedClienteIdFk : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contrato", "ClientId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contrato", "ClientId");
        }
    }
}
