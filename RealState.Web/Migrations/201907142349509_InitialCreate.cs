namespace RealState.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contrato",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                        NumeroDeParcialidades = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        Enable = c.Boolean(nullable: false),
                        Enganche = c.Double(nullable: false),
                        LoteId = c.Int(nullable: false),
                        Cliente_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Cliente_Id)
                .ForeignKey("dbo.Lote", t => t.LoteId, cascadeDelete: true)
                .Index(t => t.LoteId)
                .Index(t => t.Cliente_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Nombres = c.String(nullable: false, maxLength: 512),
                        Apellidos = c.String(nullable: false, maxLength: 512),
                        Celular = c.String(nullable: false, maxLength: 10),
                        Tipo = c.Int(nullable: false),
                        CLABE = c.String(),
                        Enable = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Lote",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(nullable: false),
                        Largo = c.Double(nullable: false),
                        Ancho = c.Double(nullable: false),
                        Latitud = c.String(),
                        Longitud = c.String(),
                        Precio = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Enable = c.Boolean(nullable: false),
                        ParcelaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Parcela", t => t.ParcelaId, cascadeDelete: true)
                .Index(t => t.ParcelaId);
            
            CreateTable(
                "dbo.Parcela",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(nullable: false),
                        Largo = c.String(nullable: false),
                        Ancho = c.String(nullable: false),
                        Enable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OdenPago",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdOpenPay = c.String(),
                        Referencia = c.String(),
                        ConvenioCIE = c.String(),
                        Importe = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Concepto = c.String(),
                        FechaVencimiento = c.DateTime(nullable: false),
                        ContratoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contrato", t => t.ContratoId, cascadeDelete: true)
                .Index(t => t.ContratoId);
            
            CreateTable(
                "dbo.Pago",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        Mensualidad = c.String(nullable: false),
                        Monto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FormaDePago = c.Int(nullable: false),
                        CLABE = c.String(),
                        Pagado = c.Boolean(nullable: false),
                        Referencia = c.String(),
                        ContratoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contrato", t => t.ContratoId, cascadeDelete: true)
                .Index(t => t.ContratoId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Pago", "ContratoId", "dbo.Contrato");
            DropForeignKey("dbo.OdenPago", "ContratoId", "dbo.Contrato");
            DropForeignKey("dbo.Contrato", "LoteId", "dbo.Lote");
            DropForeignKey("dbo.Lote", "ParcelaId", "dbo.Parcela");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Contrato", "Cliente_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Pago", new[] { "ContratoId" });
            DropIndex("dbo.OdenPago", new[] { "ContratoId" });
            DropIndex("dbo.Lote", new[] { "ParcelaId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Contrato", new[] { "Cliente_Id" });
            DropIndex("dbo.Contrato", new[] { "LoteId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Pago");
            DropTable("dbo.OdenPago");
            DropTable("dbo.Parcela");
            DropTable("dbo.Lote");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Contrato");
        }
    }
}
