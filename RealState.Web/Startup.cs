﻿using System;
using System.Threading.Tasks;
//using RealState.Notification;
using System.Configuration;
using Hangfire;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(RealState.Web.Startup))]

namespace RealState.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var sqlConnectionString = ConfigurationManager.ConnectionStrings["RealStateConnection"].ConnectionString;
            var cronExpression = ConfigurationManager.AppSettings.Get("cronExpression");
            GlobalConfiguration.Configuration.UseSqlServerStorage(sqlConnectionString);
            var environment = ConfigurationManager.AppSettings.Get("environment");

            var notification = new Notification.Notification();

            //reemplazar Cron.Minutely con la variabl cronExpression

            RecurringJob.AddOrUpdate(() => notification.SendNotifications(sqlConnectionString, environment), cronExpression);

            app.UseHangfireDashboard("/hangfire");
            app.UseHangfireServer();
        }

        public void test()
        {

        }
    }
}
