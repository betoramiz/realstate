﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealState.Web.Attributes
{
    public class NumericValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string number = value.ToString();
            int numberInt = 0;
            var canParse = int.TryParse(number, out numberInt);
            if (!canParse)
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }
}