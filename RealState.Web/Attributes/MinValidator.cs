﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealState.Web.Attributes
{
    public class MinValidator : ValidationAttribute
    {
        public double MinValue { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string number = value.ToString();
            double validNumber = 0;

            double.TryParse(number, out validNumber);
            if (validNumber < MinValue)
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }
}