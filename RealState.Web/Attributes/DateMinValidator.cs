﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace RealState.Web.Attributes
{
    public class DateMinValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string date = value.ToString();
            var validDate = new DateTime();

            CultureInfo culture = CultureInfo.CreateSpecificCulture("es-MX");
            bool DateTimeStyles  = DateTime.TryParse(date, culture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out validDate);

            //if(!isValidDate)
            //    return new ValidationResult("Este campo no tiene una Fecha Valida");

            if(validDate < DateTime.Today)
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }
}