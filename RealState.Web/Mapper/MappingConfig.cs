﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using vm = RealState.Web.ViewModels;
using models = RealState.Web.Models;
using System.Globalization;

namespace RealState.Web.Mapper
{
    public static class MappingConfig
    {
        public static MapperConfiguration InitializeAutoMapper()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                //Parcela
                cfg.CreateMap<models.Parcela, vm.Parcelas.Parcelas>()
                    .ForMember(view => view.Medidas, p => p.MapFrom(s => $"{s.Largo} x {s.Ancho}"))
                    .ForMember(view => view.Lotes, p => p.MapFrom(s => s.Lotes.Count));

                cfg.CreateMap<vm.Parcelas.ParcelaCreate, models.Parcela>()
                .BeforeMap((s, d) => d.Enable = true);

                //Lote
                cfg.CreateMap<models.Lote, vm.Lotes.Lotes>()
                    .ForMember(view => view.Medidas, p => p.MapFrom(s => $"{s.Largo} x {s.Ancho}"))
                    .ForMember(view => view.Ubicacion, p => p.MapFrom(s => $"{s.Latitud},{s.Longitud}"))
                    .ForMember(view => view.Parcela, p => p.MapFrom(s => s.Parcela.Numero));

                cfg.CreateMap<vm.Lotes.LoteCreate, models.Lote>()
                    .BeforeMap((s, d) => d.Enable = true);

                cfg.CreateMap<models.Lote, Dtos.Lotes.LoteDto>()
                    .ForMember(x => x.Id, p => p.MapFrom(s => s.Id))
                    .ForMember(x => x.Numero, p => p.MapFrom(s => s.Numero))
                    .ForMember(x => x.Precio, p => p.MapFrom(s => s.Precio));

                //Clientes
                cfg.CreateMap<models.Cliente, vm.Clientes.ClienteVM>()
                    .ForMember(view => view.NombreCompleto, p => p.MapFrom(s => $"{s.Nombres} {s.Apellidos}"))
                    .ForMember(view => view.Contratos, p => p.MapFrom(s => s.Contratos.Count))
                    .ForMember(view => view.ListaContratos, p => p.MapFrom(s => s.Contratos));

                cfg.CreateMap<vm.Clientes.ClienteCreate, models.Cliente>()
                    .ForMember(view => view.UserName, p => p.MapFrom(s => s.Email))
                    .BeforeMap((s, d) => d.Enable = true);

                cfg.CreateMap<vm.Clientes.ClienteUpdate, models.Cliente>()
                    .ForMember(view => view.UserName, p => p.MapFrom(s => s.Email))
                    .ForMember(view => view.Id, p => p.MapFrom(s => s.Id))
                    .BeforeMap((s, d) => d.Enable = true);

                //Contratos
                cfg.CreateMap<models.Contrato, Dtos.Contratos.ContratoDto>()
                    .ForMember(dto => dto.Id, model => model.MapFrom(s => s.Id.ToString()))
                    .ForMember(dto => dto.Fecha, model => model.MapFrom(s => s.Fecha.ToString("dd-MM-yyyy", CultureInfo.InvariantCulture)))
                    .ForMember(dto => dto.Cliente, model => model.MapFrom(s => $"{s.Cliente.Nombres} {s.Cliente.Apellidos}"))
                    .ForMember(dto => dto.Lote, model => model.MapFrom(s => s.Lote.Numero));

                cfg.CreateMap<models.Contrato, Dtos.Contratos.ContratoDetailDto>()
                    .ForMember(dto => dto.Id, model => model.MapFrom(s => s.Id.ToString()))
                    .ForMember(dto => dto.Nombre, model => model.MapFrom(s => s.Numero))
                    //.ForMember(dto => dto.ClienteId, model => model.MapFrom(s => s.ClienteId))
                    .ForMember(dto => dto.ClienteNombre, model => model.MapFrom(s => $"{s.Cliente.Nombres} {s.Cliente.Apellidos}"))
                    .ForMember(dto => dto.Enganche, model => model.MapFrom(s => $"{s.Enganche.ToString("C")}"))
                    .ForMember(dto => dto.Parcialidades, model => model.MapFrom(s => s.NumeroDeParcialidades))
                    .ForMember(dto => dto.LoteNombre, model => model.MapFrom(s => s.Lote.Numero))
                    .ForMember(dto => dto.Precio, model => model.MapFrom(s => s.Lote.Precio.ToString("C")));


                cfg.CreateMap<Dtos.Contratos.ContratoCreateDto, vm.Contratos.ContratoCreate>()
                    //.ForMember(view => view.Lotes, dto => dto.MapFrom(s => s.Lotes))
                    .ForMember(view => view.Parcelas, dto => dto.MapFrom(s => s.Parcelas))
                    .ForMember(view => view.Clientes, dto => dto.MapFrom(s => s.Clientes));

                cfg.CreateMap<ViewModels.Contratos.ContratoCreate, Models.Contrato>()
                    .ForMember(m => m.Numero, vm => vm.MapFrom(x => x.Nombre))
                    .ForMember(m => m.Fecha, vm => vm.MapFrom(x => DateTime.Parse(x.FechaPrimerPago)))
                    .ForMember(m => m.NumeroDeParcialidades, vm => vm.MapFrom(x => x.Mensualidades))
                    .ForMember(m => m.ClientId, vm => vm.MapFrom(x => x.ClienteId))
                    .BeforeMap((s, d) => d.Tipo = 1)//hardcoded, set types before
                    .BeforeMap((s, d) => d.Enable = true) // always enable by default
                    .BeforeMap((s, d) => d.Cliente = null)
                    .BeforeMap((s, d) => d.Lote = null);

                //Pagos
                cfg.CreateMap<Models.Pago, ViewModels.Pagos.PagosVm>()
                    .ForMember(m => m.PagoId, vm => vm.MapFrom(s => s.Id))
                    .ForMember(m => m.ContratoId, vm => vm.MapFrom(s => s.ContratoId))
                    .ForMember(m => m.ContratoNombre, vm => vm.MapFrom(s => s.Contrato.Numero))
                    //.ForMember(m => m.ClienteId, vm => vm.MapFrom(s => s.Contrato.ClienteId))
                    .ForMember(m => m.ClienteNombre, vm => vm.MapFrom(s => $"{s.Contrato.Cliente.Nombres} {s.Contrato.Cliente.Apellidos}"))
                    .ForMember(m => m.Fecha, vm => vm.MapFrom(s => s.Fecha))
                    .ForMember(m => m.Mensualidad, vm => vm.MapFrom(s => s.Mensualidad))
                    .ForMember(m => m.Monto, vm => vm.MapFrom(s => s.Monto));

                cfg.CreateMap<Models.Pago, ViewModels.Pagos.AddPagoVM>()
                    .ForMember(m => m.Nombre, source => source.MapFrom(x => $"{x.Contrato.Cliente.Nombres} {x.Contrato.Cliente.Apellidos}"))
                    .ForMember(m => m.Estatus, source => source.MapFrom(x => x.Pagado))
                    .ForMember(m => m.Monto, source => source.MapFrom(x => x.Monto));

                //ordenPago
                cfg.CreateMap<vm.OrdenPago.OrdenPagoViewModel, Models.OrdenPago>()
                    .ForMember(target => target.ContratoId, opt => opt.Ignore());
            });

            return config;
        }
    }
}