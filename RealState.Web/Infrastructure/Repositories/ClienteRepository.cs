﻿using Dto = RealState.Web.Dtos.Cliente;
using RealState.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Dapper;

namespace RealState.Web.Infrastructure.Repositories
{
    public interface IClienteRepository : IRepositoryAsync<Cliente>
    {
        Task<List<Cliente>> GetTop50Async();
        Task<List<Cliente>> GetClientByphoneAsync(string phone);
        Task<Cliente> GetByIdAsync(string id);
        List<Dto.ClienteFiltroResult> ObtenerClientesPorFiltro(string filtro);
    }

    public class ClienteRepository : RepositoryAsync<Cliente>, IClienteRepository
    {
        public ClienteRepository(Context context) : base(context) {}

        public Task<Cliente> GetByIdAsync(string id)
        {
            var cliente = db.Where(c => c.Id == id).FirstOrDefaultAsync();
            return cliente;
        }

        public async Task<List<Cliente>> GetClientByphoneAsync(string phone)
        {
            var cliente = await db.Where(c => c.Celular == phone && c.Enable == true).Include(contrato => contrato.Contratos).ToListAsync();
            return cliente;
        }

        public async Task<List<Cliente>> GetTop50Async()
        {
            var clientes = await db.Take(50).Include(c => c.Contratos).ToListAsync();
            return clientes;
        }

        public List<Dto.ClienteFiltroResult> ObtenerClientesPorFiltro(string filtro)
        {
            var connection = context.Database.Connection;
            List<Dto.ClienteFiltroResult> result = new List<Dto.ClienteFiltroResult>();

            try
            {
                if (connection.State != System.Data.ConnectionState.Open)
                    connection.Open();

                var parameters = new DynamicParameters();
                parameters.Add("@filtro", filtro, System.Data.DbType.String);

                result = connection.Query<Dto.ClienteFiltroResult>("SP_Filtros", parameters, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }
            catch (Exception)
            {
                connection.Close();
                throw;
            }

            //var morosos = new List<Dto.Moroso>();
            //morosos.Add(new Dto.Moroso() { Nombre = "Jose Perez", Contrato = "Contato 1", Parcela = "1", Lote = "1", Adeudo = "2500"  });
            //morosos.Add(new Dto.Moroso() { Nombre = "Ana Lopez", Contrato = "Contato 1", Parcela = "1", Lote = "1", Adeudo = "2500"  });
            //morosos.Add(new Dto.Moroso() { Nombre = "Pedro Jesus", Contrato = "Contato 1", Parcela = "1", Lote = "1", Adeudo = "2500"  });

            //return morosos;

            return result;
        }
    }
}