﻿using RealState.Web.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RealState.Web.Infrastructure.Repositories
{
    public interface ILoteRepository : IRepositoryAsync<Lote>
    {
        Task<List<Lote>> GetByNumberAsync(string number);
        Task<List<Lote>> GetTop50LotesAsync();
        Task<Lote> GetByIdAsyncAndParcela(int id);
        Task<List<Lote>> GetListOfEnablesAsync();
        Task<List<Lote>> GetForParcela(int parcelaId);
    }

    public class LoteRepository : RepositoryAsync<Lote>, ILoteRepository
    {
        public LoteRepository(Context context) : base(context)
        {

        }

        public async Task<List<Lote>> GetByNumberAsync(string number)
        {
            var lotes = await db.Where(x => x.Numero == number).Include(p => p.Parcela).AsNoTracking().ToListAsync();
            return lotes;
        }

        public async Task<Lote> GetByIdAsyncAndParcela(int id)
        {
            var lote = await db.Where(x => x.Id == id).Include(p => p.Parcela).AsNoTracking().FirstOrDefaultAsync();
            return lote;
        }

        public async Task<List<Lote>> GetTop50LotesAsync()
        {
            var lotes = await db.Take(50).Include(p => p.Parcela).AsNoTracking().ToListAsync();
            return lotes;
        }

        public async Task<List<Lote>> GetListOfEnablesAsync()
        {
            var lotes = await db.Where(x => x.Enable == true).ToListAsync();
            return lotes;
        }

        public async Task<List<Lote>> GetForParcela(int parcelaId)
        {
            var lotes = await db.Where(x => x.ParcelaId == parcelaId && x.Enable == true).ToListAsync();
            return lotes;
        }
    }
}