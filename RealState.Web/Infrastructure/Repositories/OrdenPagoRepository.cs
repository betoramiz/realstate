﻿using RealState.Web.Models;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RealState.Web.Infrastructure.Repositories
{
    public interface IOrdenPagoRepository : IRepositoryAsync<OrdenPago>
    {
        Task<OrdenPago> OrdenPagoPorcontrato(int contratoId);
    }

    public class OrdenPagoRepository : RepositoryAsync<OrdenPago>, IOrdenPagoRepository
    {
        public OrdenPagoRepository(Context context) : base(context)
        {

        }

        public async Task<OrdenPago> OrdenPagoPorcontrato(int contratoId)
        {
            var ordenPago = await db.Where(orden => orden.ContratoId == contratoId).FirstOrDefaultAsync();
            return ordenPago;
        }
    }
}