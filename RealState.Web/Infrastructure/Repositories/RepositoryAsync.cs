﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RealState.Web.Infrastructure.Repositories
{
    public class RepositoryAsync<T> : IRepositoryAsync<T> where T : class
    {
        protected Context context;
        protected DbSet<T> db;

        public RepositoryAsync(Context context)
        {
            this.context = context;
            db = context.Set<T>();
        }

        public async Task Create(T entity)
        {
            db.Add(entity);
            await context.SaveChangesAsync();
        }

        public async Task Delete(T entity)
        {
            db.Remove(entity);
            await context.SaveChangesAsync();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            var entity = await db.FindAsync(id);
            return entity;
        }

        public async Task Update(T entity)
        {
            bool saveFailed;
            do
            {
                saveFailed = false;

                try
                {
                    context.Entry(entity).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    ex.Entries.Single().Reload();
                }
            }while (saveFailed);
        }
    }
}