﻿using Dapper;
using RealState.Web.Enums;
using RealState.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RealState.Web.Infrastructure.Repositories
{
    public interface IPagoRepository : IRepositoryAsync<Pago>
    {
        void AddPago(int Id, string environment);
        Task<List<Pago>> GetPayListCurrentMonth();
        Task<List<Pago>> GetPayListInMont(Months month);
        Task<Pago> GetDetailByIdAsync(int id);
        Task<List<Pago>> GetPagosByContractId(int id);
        Task<List<Pago>> GetPagosVencidosByContractId(int id);
    }

    public class PagoRepository : RepositoryAsync<Pago>, IPagoRepository
    {
        public PagoRepository(Context context) : base(context)
        {

        }

        public void AddPago(int Id, string environment)
        {
            var connection = context.Database.Connection;
            try
            {
                if (connection.State != System.Data.ConnectionState.Open)
                    connection.Open();

                var parameter = new DynamicParameters();
                parameter.Add("Contrato", Id, System.Data.DbType.Int32);

                if(environment != "Release")
                    parameter.Add("Pruebas", 1, System.Data.DbType.Int32);

                var contratoVmItems = connection.Execute("InsertaPagos", parameter, commandType: System.Data.CommandType.StoredProcedure);

                connection.Close();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw;
            }
        }

        public async Task<Pago> GetDetailByIdAsync(int id)
        {
            return await db.Where(x => x.Id == id).Include(c => c.Contrato).Include(cc => cc.Contrato.Cliente).FirstOrDefaultAsync();
        }

        public async Task<List<Pago>> GetPagosByContractId(int id = 0)
        {
            return await db.Where(x => x.ContratoId == id).ToListAsync();
        }

        public async Task<List<Pago>> GetPagosVencidosByContractId(int id = 0)
        {
            var currentDate = DateTime.Today;
            return await db.Where(x => x.ContratoId == id && x.Fecha.Month <= currentDate.Month && x.Fecha.Year <= currentDate.Year && x.Pagado == false).ToListAsync();
        }

        public async Task<List<Pago>> GetPayListCurrentMonth()
        {
            return await GetPagos();
        }

        public async Task<List<Pago>> GetPayListInMont(Months month)
        {
            return await GetPagos(month);
        }

        private async Task<List<Pago>> GetPagos(Months month = 0)
        {
            int currentMonth = (int)month == 0 ? DateTime.Now.Month : (int)month;
            var currentYear = DateTime.Now.Year;
            var pagoList = db.Where(x => x.Fecha.Month == currentMonth && x.Fecha.Year == currentYear && x.Pagado == false)
                .OrderByDescending(x => x.Fecha)
                .Include(c => c.Contrato)
                .Include(cl => cl.Contrato.Cliente)
                .ToListAsync();
            return await pagoList;
        }
    }
}