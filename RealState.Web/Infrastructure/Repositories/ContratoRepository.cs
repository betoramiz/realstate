﻿using RealState.Web.Dtos.Contratos;
using RealState.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Dapper;

namespace RealState.Web.Infrastructure.Repositories
{
    public interface IContratoRepository : IRepositoryAsync<Contrato>
    {
        Task<List<Contrato>> Get50FirstAsync();
        ContratoCreateDto GetContratoCreteItems();
        Task<List<Contrato>> GetListByNameAsync(string name);
        Task<Contrato> GetByNameAsync(string name);
        Task<bool> ExisitContract(string name);
        Task<Contrato> GetContrato(int id);
        Task<List<ContratoDebtDto>> GetAdeudos(int contractId);
        void CreateByProcedure(Contrato contrato);
        bool CreateWebhook(Webhook webhook, ref int Contrato_Id, ref string Email, ref string Telefono, ref string Nombre);
        Task<ContratoIndicadores> GetIndicadores();
    }

    public class ContratoRepository : RepositoryAsync<Contrato>, IContratoRepository
    {
        public ContratoRepository(Context context) : base(context)
        {

        }

        public async Task<bool> ExisitContract(string name)
        {
            return await db.AnyAsync(x => x.Numero == name);
        }

        public async Task<List<Contrato>> Get50FirstAsync()
        {
            var contracts = await db.AsNoTracking().Take(50).Include(c => c.Cliente).Include(l => l.Lote).ToListAsync();

            return contracts;
        }

        public async Task<List<Contrato>> GetListByNameAsync(string name)
        {
            return await db.Where(x => x.Numero == name).ToListAsync();
        }

        public ContratoCreateDto GetContratoCreteItems()
        {
            var connection = context.Database.Connection;
            try
            {
                if (connection.State != System.Data.ConnectionState.Open)
                    connection.Open();

                var contratoVmItems = connection.QueryMultiple("CreateContractViewModel", commandType: System.Data.CommandType.StoredProcedure);

                var parcelas = contratoVmItems.Read<ContratoParcelaDto>().ToList();
                var clientes = contratoVmItems.Read<ContratoClienteDto>().ToList();

                connection.Close();

                var contratoCreateDto = new ContratoCreateDto()
                {
                    Clientes = clientes,
                    Parcelas = parcelas
                };

                return contratoCreateDto;
            }
            catch (Exception ex)
            {
                connection.Close();
                throw;
            }
        }

        public async Task<Contrato> GetByNameAsync(string name)
        {
            return await db.Where(x => x.Numero == name).Include(c => c.Cliente).Include(l => l.Lote).FirstOrDefaultAsync();
        }

        public void CreateByProcedure(Contrato contrato)
        {
            var connection = context.Database.Connection;
            try
            {
                if (connection.State != System.Data.ConnectionState.Open)
                    connection.Open();

                var parameters = new DynamicParameters();
                parameters.Add("@numero", contrato.Numero, System.Data.DbType.String);
                parameters.Add("@fecha", contrato.Fecha, System.Data.DbType.DateTime);
                parameters.Add("@tipo", contrato.Tipo, System.Data.DbType.Int32);
                parameters.Add("@enable", contrato.Enable, System.Data.DbType.Boolean);
                parameters.Add("@enganche", contrato.Enganche, System.Data.DbType.Double);
                parameters.Add("@lote", contrato.LoteId, System.Data.DbType.Int32);
                parameters.Add("@numeroParcialidades", contrato.NumeroDeParcialidades, System.Data.DbType.Int32);
                parameters.Add("@cliente", contrato.ClientId, System.Data.DbType.String);


                connection.Execute("CreateContrato", parameters , commandType: System.Data.CommandType.StoredProcedure);
            }
            catch (Exception)
            {
                connection.Close();
                throw;
            }

        }

        public bool CreateWebhook(Webhook webhook, ref int Contrato_Id, ref string Email, ref string Telefono, ref string Nombre)
        {
            var connection = context.Database.Connection;
            try
            {
                if (connection.State != System.Data.ConnectionState.Open)
                    connection.Open();

                var parameters = new DynamicParameters();
                parameters.Add("@type", webhook.type, System.Data.DbType.String);
                parameters.Add("@event_date", webhook.event_date, System.Data.DbType.String);
                parameters.Add("@verification_code", webhook.verification_code, System.Data.DbType.String);
                parameters.Add("@transaction_amount", webhook.transaction_amount, System.Data.DbType.String);
                parameters.Add("@transaction_authorization", webhook.transaction_authorization, System.Data.DbType.String);
                parameters.Add("@transaction_method", webhook.transaction_method, System.Data.DbType.String);
                parameters.Add("@transaction_operation_type", webhook.transaction_operation_type, System.Data.DbType.String);
                parameters.Add("@transaction_transaction_type", webhook.transaction_transaction_type, System.Data.DbType.String);
                parameters.Add("@transaction_card_type", webhook.transaction_card_type, System.Data.DbType.String);
                parameters.Add("@transaction_card_brand", webhook.transaction_card_brand, System.Data.DbType.String);
                parameters.Add("@transaction_card_address_line1", webhook.transaction_card_address_line1, System.Data.DbType.String);
                parameters.Add("@transaction_card_address_line2", webhook.transaction_card_address_line2, System.Data.DbType.String);
                parameters.Add("@transaction_card_address_line3", webhook.transaction_card_address_line3, System.Data.DbType.String);
                parameters.Add("@transaction_card_address_state", webhook.transaction_card_address_state, System.Data.DbType.String);
                parameters.Add("@transaction_card_address_city", webhook.transaction_card_address_city, System.Data.DbType.String);
                parameters.Add("@transaction_card_address_postal_code", webhook.transaction_card_address_postal_code, System.Data.DbType.String);
                parameters.Add("@transaction_card_address_country_code", webhook.transaction_card_address_country_code, System.Data.DbType.String);
                parameters.Add("@transaction_card_card_number", webhook.transaction_card_card_number, System.Data.DbType.String);
                parameters.Add("@transaction_card_holder_name", webhook.transaction_card_holder_name, System.Data.DbType.String);
                parameters.Add("@transaction_card_expiration_month", webhook.transaction_card_expiration_month, System.Data.DbType.String);
                parameters.Add("@transaction_card_expiration_year ", webhook.transaction_card_expiration_year, System.Data.DbType.String);
                parameters.Add("@transaction_card_allows_charges  ", webhook.transaction_card_allows_charges, System.Data.DbType.String);
                parameters.Add("@transaction_card_allows_payouts", webhook.transaction_card_allows_payouts, System.Data.DbType.String);
                parameters.Add("@transaction_card_creation_date", webhook.transaction_card_creation_date, System.Data.DbType.String);
                parameters.Add("@transaction_card_bank_name", webhook.transaction_card_bank_name, System.Data.DbType.String);
                parameters.Add("@transaction_card_bank_code", webhook.transaction_card_bank_code, System.Data.DbType.String);
                parameters.Add("@transaction_status", webhook.transaction_status, System.Data.DbType.String);
                parameters.Add("@transaction_id", webhook.transaction_id, System.Data.DbType.String);
                parameters.Add("@transaction_creation_date", webhook.transaction_creation_date, System.Data.DbType.String);
                parameters.Add("@transaction_description  ", webhook.transaction_description, System.Data.DbType.String);
                parameters.Add("@transaction_error_message", webhook.transaction_error_message, System.Data.DbType.String);
                parameters.Add("@transaction_order_id", webhook.transaction_order_id, System.Data.DbType.String);
                parameters.Add("@Contrato_Id", 0, System.Data.DbType.Int32, System.Data.ParameterDirection.Output);
                parameters.Add("@Telefono", 0, System.Data.DbType.String, System.Data.ParameterDirection.Output, 10);
                parameters.Add("@Email", 0, System.Data.DbType.String, System.Data.ParameterDirection.Output, 256);
                parameters.Add("@Nombre", 0, System.Data.DbType.String, System.Data.ParameterDirection.Output, 1024);

                var regreso = connection.Execute("CreateWebhook", parameters, commandType: System.Data.CommandType.StoredProcedure);

                Contrato_Id = parameters.Get<int>("@Contrato_Id");
                Telefono = parameters.Get<string>("@Telefono");
                Email = parameters.Get<string>("@Email");
                Nombre = parameters.Get<string>("@Nombre");

                return regreso > 0;
            }
            catch (Exception)
            {
                connection.Close();
                throw;
            }

        }

        public async Task<Contrato> GetContrato(int id)
        {
            var contrato = await db.Where(x => x.Id == id).Include(c => c.Cliente).Include(l => l.Lote).Include(ord => ord.Ordenes).FirstOrDefaultAsync();

            return contrato;
        }

        public async Task<List<ContratoDebtDto>> GetAdeudos(int contractId)
        {
            var currentMonth = DateTime.Today.Month;
            var currentYear = DateTime.Today.Year;

            var debs = new List<ContratoDebtDto>();
            debs = await (from p in context.Pago
                        where (p.Fecha.Month <= currentMonth && p.Fecha.Year <= currentYear) 
                            && p.ContratoId == contractId 
                            && p.Pagado == false
                        select new ContratoDebtDto
                        {
                            PagoId = p.Id,
                            ContratoId = p.ContratoId,
                            FechaPago = p.Fecha,
                            Mensualidad = p.Mensualidad,
                            Monto = p.Monto
                        }).ToListAsync();

            return debs;

        }

        public async Task<Dtos.Contratos.ContratoIndicadores> GetIndicadores()
        {
            ContratoIndicadores indicadores = new ContratoIndicadores();

            var connection = context.Database.Connection;
            try
            {
                if (connection.State != System.Data.ConnectionState.Open)
                    connection.Open();

                var parameters = new DynamicParameters();
                parameters.Add("@Morosos", indicadores.Morosos, System.Data.DbType.Int32, System.Data.ParameterDirection.Output);
                parameters.Add("@Puntuales", indicadores.Puntuales, System.Data.DbType.Int32, System.Data.ParameterDirection.Output);
                parameters.Add("@Liquidaciones", indicadores.Liquidaciones, System.Data.DbType.Int32, System.Data.ParameterDirection.Output);

                connection.Execute("GetIndicadores", parameters, commandType: System.Data.CommandType.StoredProcedure);

                indicadores.Morosos = parameters.Get<int>("@Morosos");
                indicadores.Puntuales = parameters.Get<int>("@Puntuales");
                indicadores.Liquidaciones = parameters.Get<int>("@Liquidaciones");

                return indicadores;
            }
            catch (Exception)
            {
                connection.Close();
                throw;
            }
        }
    }
}