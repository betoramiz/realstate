﻿using RealState.Web.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RealState.Web.Infrastructure.Repositories
{
    public interface IParcelaRepository : IRepositoryAsync<Parcela>
    {
        Task<List<Parcela>> GetListAsync();
        Task<List<Parcela>> GetByNumberAsync(string number);
        Task<Parcela> GetByIdAsyncAndIncludeLotes(int id);
    }

    public class ParcelaRepository : RepositoryAsync<Parcela>, IParcelaRepository
    {
        public ParcelaRepository(Context context) : base(context)
        {

        }

        public async Task<List<Parcela>> GetByNumberAsync(string number)
        {
            var parcela = await db.Where(x => x.Numero == number).AsNoTracking().ToListAsync();
            return parcela;
        }

        public async Task<List<Parcela>> GetListAsync()
        {
            return await db.Include(l => l.Lotes).ToListAsync();
        }

        public async Task<Parcela> GetByIdAsyncAndIncludeLotes(int id)
        {
            var parcela = await db.Where(x => x.Id == id).Include(l => l.Lotes).FirstOrDefaultAsync();
            return parcela;
        }
    }
}