﻿using Microsoft.AspNet.Identity.EntityFramework;
using RealState.Web.Models;
using System.Data.Entity;

namespace RealState.Web.Infrastructure
{
    public class Context : IdentityDbContext<Cliente>
    {
        public DbSet<Contrato> Contrato { get; set; }
        public DbSet<Lote> Lote { get; set; }
        public DbSet<Pago> Pago { get; set; }
        public DbSet<Parcela> Parcela { get; set; }
        public DbSet<OrdenPago> OrdenPago { get; set; }

        public Context() : base("name=RealStateConnection")
        {

        }
    }
}