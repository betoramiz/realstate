﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RealState.Web.Infrastructure
{
    public interface IRepositoryAsync<T> where T : class
    {
        Task Create(T entity);
        Task<T> GetByIdAsync(int id);
        Task Update(T entity);
        Task Delete(T entity);
    }
}
