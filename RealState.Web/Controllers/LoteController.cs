﻿using RealState.Web.Models;
using RealState.Web.Services;
using RealState.Web.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RealState.Web.Controllers
{
    public class LoteController : Controller
    {
        private ILoteService loteService;
        private IParcelaService parcelaService;
        public LoteController(ILoteService loteService, IParcelaService parcelaService)
        {
            this.loteService = loteService;
            this.parcelaService = parcelaService;
        }

        [HttpGet]
        public async Task<ActionResult> Lista(string number = "")
        {
            List<ViewModels.Lotes.Lotes> lotes = new List<ViewModels.Lotes.Lotes>();

            if (!string.IsNullOrEmpty(number) && !string.IsNullOrWhiteSpace(number))
                lotes =  await loteService.BuscarPorNumeroAsync(number);
            else
                lotes = await loteService.GetFirst50Async();

            return View(lotes);
        }

        [HttpGet]
        public async Task<ActionResult> Agregar()
        {
            ViewModels.Lotes.LoteCreate lote = new ViewModels.Lotes.LoteCreate();
            var parcelas = await parcelaService.GetListParcelasAsync();
            lote.Parcelas = parcelas;
            
            return View(lote);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Agregar(ViewModels.Lotes.LoteCreate lote)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(lote);

                await loteService.CreateAsync(lote);

                return RedirectToAction("Lista");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(lote);
            }
        }

        [HttpGet]
        public async Task<ActionResult> Detalle(int id)
        {
            var lote = await loteService.GetByIdAsync(id);
            if (lote == null)
                throw new Exception("El lote no fue encontrado");

            return View(lote);
        }
    }
}