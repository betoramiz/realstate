﻿using RealState.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RealState.Web.Controllers
{
    [RoutePrefix("api")]
    public class RealStateController : ApiController
    {

        private IContratoService contratoService;
        private IParcelaService parcelaService;
        private ILoteService loteService;

        public RealStateController(IContratoService contratoService, IParcelaService parcelaService, ILoteService loteService)
        {
            this.contratoService = contratoService;
            this.parcelaService = parcelaService;
            this.loteService = loteService;
        }

        [HttpGet]
        [Route("parcelas")]
        public IHttpActionResult ObtenParcelas()
        {
            return Ok();
        }
    }
}
