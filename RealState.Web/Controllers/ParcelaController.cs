﻿using RealState.Web.Services;
using viewModel = RealState.Web.ViewModels;
using System.Threading.Tasks;
using System.Web.Mvc;
using System;

namespace RealState.Web.Controllers
{
    public class ParcelaController : Controller
    {
        private IParcelaService parcelaService;
        public ParcelaController(IParcelaService parcelaService)
        {
            this.parcelaService = parcelaService;
        }

        [HttpGet]
        public async Task<ActionResult> Lista()
        {
            var parcelas = await parcelaService.GetListAsync();
            return View(parcelas);
        }

        [HttpGet]
        public ActionResult Agregar()
        {
            var parcela = new viewModel.Parcelas.ParcelaCreate();
            return View(parcela);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Agregar(viewModel.Parcelas.ParcelaCreate parcela)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(parcela);

                await parcelaService.CreateAsync(parcela);

                return RedirectToAction("Lista");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(parcela);
            }
            
        }

        [HttpGet]
        public async Task<ActionResult> Detalle(int id)
        {
            var parcela = await parcelaService.FindByIdAsync(id);

            if (parcela == null)
                throw new Exception("No se pude encontrar el recurso");

            return View(parcela);
        }
    }
}