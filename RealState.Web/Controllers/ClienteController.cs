﻿using RealState.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RealState.Web.Controllers
{
    public class ClienteController : Controller
    {
        private IClienteService clienteService;

        public ClienteController(IClienteService clienteService)
        {
            this.clienteService = clienteService;
        }
        
        [HttpGet]
        public async Task<ActionResult> Lista(string celular)
        {
            var clientes = new List<ViewModels.Clientes.ClienteVM>();
            if(!string.IsNullOrEmpty(celular) || !string.IsNullOrWhiteSpace(celular))
                clientes = await clienteService.GetClientByPhone(celular);
            else
                clientes = await clienteService.ListaClientesAsync();

            return View(clientes);
        }

        [HttpGet]
        public ActionResult Agregar()
        {
            var cliente = new ViewModels.Clientes.ClienteCreate();
            return View(cliente);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Agregar(ViewModels.Clientes.ClienteCreate cliente)
        {
            if (!ModelState.IsValid)
                return View(cliente);

            try
            {
                await clienteService.CreateAsync(cliente);
                return RedirectToAction("Lista");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(cliente);
            }
        }

        [HttpGet]
        public async Task<ActionResult> Modificar(string id)
        {
            var cliente = await clienteService.GetClienteByIdForUpdate(id);
            if (cliente == null)
                throw new Exception($"El cliete que intenta modificar no existe en el sistema");

            return View(cliente);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Modificar(ViewModels.Clientes.ClienteUpdate cliente)
        {
            if (!ModelState.IsValid)
                return View(cliente);

            try
            {
                await clienteService.UpdateAsync(cliente);

                return RedirectToAction("Lista");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(cliente);
            }
        }
    }
}