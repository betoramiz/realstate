﻿using RealState.Web.Enums;
using RealState.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RealState.Web.Controllers
{
    public class PagosController : Controller
    {
        #region Propierties
        private IPagoService pagoService;
        #endregion

        public PagosController(IPagoService pagoService)
        {
            this.pagoService = pagoService;
        }

        public async Task<ActionResult> Lista(string mes)
        {
            ViewBag.Month = string.Empty;
            var pagos = new List<ViewModels.Pagos.PagosVm>();
            if(!string.IsNullOrEmpty(mes) && !string.IsNullOrWhiteSpace(mes) && mes != "0")
            {
                Months month = (Months)int.Parse(mes);
                pagos = await pagoService.PagosinMonth(month);
                ViewBag.Month = month.ToString();
            }
            else
            {
                pagos = await pagoService.PagosCurrentMonth();
                Months month = (Months)DateTime.Now.Month;
                ViewBag.Month = month.ToString();
            }
            return View(pagos);
        }

        public async Task<ActionResult> Agregar(int id)
        {
            var pago = await pagoService.GetDetailByIdAsync(id);
            return View(pago);
        }

        [HttpPost]
        public async Task<ActionResult> Agregar(ViewModels.Pagos.AddPagoVM altaPago)
        {
            var pago  = await pagoService.AddPayments(altaPago.Id);
            return RedirectToAction("Lista", new { mes = "0" });
        }
    }
}