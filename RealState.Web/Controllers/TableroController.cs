﻿using RealState.Web.Dtos.Cliente;
using RealState.Web.Infrastructure.Repositories;
using RealState.Web.Services;
using RealState.Web.Services.Implementation;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace RealState.Web.Controllers
{
    public class TableroController : Controller
    {
        ContratoService contratoService = new ContratoService(new ContratoRepository(new Infrastructure.Context()), null);

        private ITableroService tableroService;
        public TableroController(ITableroService tableroService)
        {
            this.tableroService = tableroService;
        }

        // GET: Tablero
        public async Task<ActionResult> Inicio()
        {
            var indicadores = await contratoService.GetIndicadores();
            return View(indicadores);
        }

        public PartialViewResult ObtenerClientesPorfiltro(string filtro)
        {

            List<ClienteFiltroResult> results = tableroService.ObtenerClientePorFiltro(filtro);

            return PartialView("~/Views/Tablero/MorososPartialView.cshtml", results);
        }
    }
}