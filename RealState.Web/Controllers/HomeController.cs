﻿using Firebase.Auth;
using RealState.Web.Models;
using System;
using System.Configuration;
using System.Runtime.Caching;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace RealState.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [HttpGet]
        public ActionResult Index()
        {
            var User = Session["FirebaseUser"] as User;

            if (User != null)
            {
                Response.Redirect(Url.Action("Inicio", "Tablero"));
            }

            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            if (!(Session["FirebaseUser"] is User User))
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Remove("FirebaseUser");
            Response.Redirect(Url.Action("Index", "Home"));
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Index(Login model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Intento de inicio de sesión no válido.");
                return View(model);
            }

            FirebaseAuthProvider objProvider = new FirebaseAuthProvider(new FirebaseConfig(ConfigurationManager.AppSettings["Firebase-Key"]));

            FirebaseAuthLink resultado = new FirebaseAuthLink(objProvider, new FirebaseAuth());

            try
            {
                resultado = objProvider.SignInWithEmailAndPasswordAsync(model.Email, model.Contrasena).Result;
            }
            catch (Exception ex)
            {
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                var _result = json_serializer.Deserialize<Error>(((Firebase.Auth.FirebaseAuthException)ex.InnerException).ResponseData);

                ModelState.AddModelError("", _result.error.message);
                return View(model);
            }

            Session.Remove("FirebaseUser");
            Session.Add("FirebaseUser", resultado.User);
            //Response.Redirect(Url.Action("Inicio", "Tablero"));

            return Redirect(Url.Action("Inicio", "Tablero"));
        }
    }

    public class Error
    {
        public Code error;
    }

    public class Code
    {
        public string code;
        public string message;
    }

    public static class CacheConfiguracion
    {
        public static CacheItemPolicy cip = new CacheItemPolicy() { AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddMinutes(30)) };
        public static CacheItemPolicy cip7dias = new CacheItemPolicy() { AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddDays(7)) };
        public static CacheItemPolicy cip5dias = new CacheItemPolicy() { AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddDays(5)) };
    }

}