﻿using RealState.Web.Services;
using RealState.Web.ViewModels.OrdenPago;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RealState.Web.Controllers
{
    public class BancoController : Controller
    {
        public IClienteService clienteService;
        public IBancoService bancoService;
        public IContratoService contratoService;
        public IPagoService pagoService;
        public IOrdenPagoService ordenPagoService;

        public BancoController(IClienteService clienteService, IBancoService bancoService, IContratoService contratoService, IPagoService pagoService, IOrdenPagoService ordenPagoService)
        {
            this.clienteService = clienteService;
            this.bancoService = bancoService;
            this.contratoService = contratoService;
            this.pagoService = pagoService;
            this.ordenPagoService = ordenPagoService;
        }

        public ActionResult Generardor()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Lista(string celular)
        {
            var clientes = new List<ViewModels.Clientes.ClienteVM>();
            if (!string.IsNullOrEmpty(celular) || !string.IsNullOrWhiteSpace(celular))
                clientes = await clienteService.GetClientWithContract(celular);
            else
                clientes = await clienteService.ListaClientesWithContactAsync();

            return View(clientes);
        }

        [HttpGet]
        public async Task<PartialViewResult> OrdenPago(int contratoId = 0)
        {
            var contratoDetail = await contratoService.GetContratoAndordenes(contratoId);
            if (contratoDetail == null)
                return PartialView("OrdenPago", "ErrorOrdenPago");

            string ultimaOrdenId = string.Empty;
            if(contratoDetail.Ordenes.Count > 0)
            {
                var ultimaOrden = contratoDetail.Ordenes.OrderByDescending(orden => orden.FechaVencimiento).First();
                ultimaOrdenId = ultimaOrden.Id.ToString();
                var fechaActual = DateTime.Today;
                if (DateTime.Compare(fechaActual, ultimaOrden.FechaVencimiento) <= 0)
                {
                    //obtener orde de pago y mostrarla en pantalla
                    var ordenPagodatos = await ordenPagoService.ObtenOrdenPagoPorContrato(contratoId);
                    Dtos.Banco.Banco banco = new Dtos.Banco.Banco()
                    {
                        PaymentMethod = new Dtos.Banco.PaymentMethod()
                        {
                            Agreement = ordenPagodatos.ConvenioCIE,
                            Name = ordenPagodatos.Referencia,
                        },
                        Amount = ordenPagodatos.Importe,
                        Description = ordenPagodatos.Concepto
                    };
                    return PartialView("OrdenPago", banco);
                }
            }

            var deuda = await contratoService.GetPayAmount(contratoId);
            var cliente = await clienteService.GetClienteByIdOpenPay(contratoDetail.ClienteId);

            ultimaOrdenId = ultimaOrdenId == string.Empty ? 1.ToString() : ultimaOrdenId;
            var cargo = bancoService.GeneraPago(deuda.Monto, cliente, ultimaOrdenId);
            var fechaExpiracion = cargo.CreationDate.AddDays(30);
            var ordenPago = new OrdenPagoViewModel()
            {
                ConvenioCIE = cargo.PaymentMethod.Agreement,
                Concepto = cargo.Description,
                IdOpenPay = cargo.Id,
                Importe = cargo.Amount,
                Referencia = cargo.PaymentMethod.Name,
                FechaVencimiento = fechaExpiracion
            };

            if(!string.IsNullOrEmpty(cargo.ErrorMessage))
                return PartialView("OrdenPago", "ErrorOrdenPago");
            await ordenPagoService.GuardaOrden(ordenPago, contratoId);


            return PartialView(deuda);
        }

        //[HttpGet]
        //public Task<PartialViewResult> GenerarPago()
        //{

        //    return PartialView();
        //}
    }
}