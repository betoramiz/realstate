﻿using RealState.Web.Dtos.General;
using RealState.Web.Services;
using RealState.Web.ViewModels.Contratos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Linq;

namespace RealState.Web.Controllers
{
    public class ContratoController : Controller
    {

        private IContratoService contratoService;
        private IPagoService pagoService;
        private IParcelaService parcelaService;
        private ILoteService loteService;
        private RealState.Notification.Notification mensajeSender = new RealState.Notification.Notification();

        public ContratoController(IContratoService contratoService, IPagoService pagoService, IParcelaService parcelaService, ILoteService loteService)
        {
            this.contratoService = contratoService;
            this.pagoService = pagoService;
            this.parcelaService = parcelaService;
            this.loteService = loteService;
        }


        public async Task<ActionResult> Lista(string nombre)
        {
            var contratos = new List<Dtos.Contratos.ContratoDto>();
            if (!string.IsNullOrEmpty(nombre) && !string.IsNullOrWhiteSpace(nombre))
            {
                var contrato  = await contratoService.GetContratoByName(nombre);

                if (contrato != null)
                    contratos.Add(contrato);
            }
            else
                contratos = await contratoService.ListaAsync();
            return View(contratos);
        }

        [HttpGet]
        public ActionResult Agregar()
        {
            var createContract = new ContratoCreate();
            var items = contratoService.GetContratoCreateItems();
            Session["items"] = items;
            return View(items);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Agregar(ContratoCreate contrato)
        {
            if (!ModelState.IsValid)
            {
                var items = contratoService.GetContratoCreateItems();
                contrato.Clientes = items.Clientes;
                contrato.Parcelas = items.Parcelas;

                return View(contrato);
            }
            try
            {
                await contratoService.AddContractAsync(contrato);

                var contratoDto = await contratoService.GetContratoByName(contrato.Nombre);

                var contratoId = int.Parse(contratoDto.Id);

                string environment = ConfigurationManager.AppSettings.Get("Environment");
                pagoService.AddPago(contratoId, environment);


                var sqlConnectionString = ConfigurationManager.ConnectionStrings["RealStateConnection"].ConnectionString;


                var items = Session["items"] as ContratoCreate;
                var Nombre = items.Clientes.ToList().Where(x=>x.Id == contrato.ClienteId).FirstOrDefault().NombreCompleto;
                var Email = items.Clientes.ToList().Where(x => x.Id == contrato.ClienteId).FirstOrDefault().Email;
                var Celular = items.Clientes.ToList().Where(x => x.Id == contrato.ClienteId).FirstOrDefault().Celular;

                if(environment == "Release")
                    mensajeSender.SendNotifications(sqlConnectionString, contrato.Nombre, Email, Celular, Nombre, contratoDto.Lote, string.Empty);

                ViewBag.Message = "Contrato Agregado Correctamente";
                return RedirectToAction("Lista");
            }
            catch (Exception ex)
            {
                var items = contratoService.GetContratoCreateItems();
                contrato.Clientes = items.Clientes;
                contrato.Parcelas = items.Parcelas;

                ModelState.AddModelError("", "No se pudo dar de alta el contrato, intentelo de nuevo");
                return View(contrato);
            }
        }


        [HttpGet]
        public async Task<ActionResult> Detalle(int? id)
        {
            try
            {
                if (!id.HasValue)
                    throw new Exception($"El id {id} no pudo ser encontrado");

                var contrato = await contratoService.GetContratoById(id.Value);
                return View(contrato);
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        [HttpGet]
        public async Task<JsonResult> GetLotes(int? parcela)
        {
            try
            {
                if (!parcela.HasValue)
                    return Json( new Response { success = false, Message = "Parcela Id es necesario", Data = null }, JsonRequestBehavior.AllowGet );

                var lotes = await loteService.GetForParcelas(parcela.Value);
                return Json(new Response { success = true, Message = "", Data = lotes }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new Response { success = false, Message = ex.Message, Data = null }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}