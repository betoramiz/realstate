﻿using RealState.Web.Services;
using RealState.Web.Services.Implementation;
using RealState.Web.ViewModels.Contratos;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http;

namespace RealState.Web
{
    public class PagoController : ApiController
    {
        WebhookService webhookService = new WebhookService();

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        async public Task<IHttpActionResult> Post([FromBody]Webhook webhook)
        {
            int Contrato_Id = 0;
            string Email = "";
            string Telefono = "";
            string Nombre = "";

            switch (webhook.type)
            {
                case Webhook.VERIFICATION:
                    webhook.transaction = new Transaccion() {
                        card = new Card() {
                            address = new Address()
                        }
                    };
                    webhookService.AddWebhookAsync(webhook, ref Contrato_Id, ref Email, ref Telefono, ref Nombre);
                    break;
                default:
                    webhookService.AddWebhookAsync(webhook, ref Contrato_Id, ref Email, ref Telefono, ref Nombre);
                    if(Contrato_Id > 0)
                    {
                        var notification = new Notification.Notification();
                        var sqlConnectionString = ConfigurationManager.ConnectionStrings["RealStateConnection"].ConnectionString;
                        notification.SendNotifications(sqlConnectionString, Contrato_Id, Email, Telefono, Nombre, new decimal(webhook.transaction.amount));
                    }
                    break;
            }
            return Ok();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }


}