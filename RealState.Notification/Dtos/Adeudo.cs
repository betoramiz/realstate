﻿using System;

namespace RealState.Notification.Dtos
{
    public class Adeudo
    {
        public int PagoId { get; set; }
        public int ContratoId { get; set; }
        public string Contrato { get; set; }
        public string ClienteId { get; set; }
        public string Cliente { get; set; }
        public string CorreoElectronico { get; set; }
        public string Celular { get; set; }
        public decimal Total { get; set; }
        public decimal Monto { get; set; }
        public int MensualidadId { get; set; }
        public DateTime FechaPago { get; set; }
        public string Referencia { get; set; }
    }
}
