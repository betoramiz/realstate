﻿using System;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using RealState.Notification.Dtos;
using System.Collections.Generic;
using System.Configuration;

namespace RealState.Notification
{
    public class Sqloperations
    {

        private string SqlConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["RealStateConnection"].ConnectionString;
            }
        }

        public List<Adeudo> ObtenerAdeudos(int anticipacion = 0)
        {
            SqlConnection connection = new SqlConnection(SqlConnectionString);
            List<Adeudo> adeudos = new List<Adeudo>();
            try
            {
                connection.Open();

                var storedProcedure = "SP_ObtenerAdeudos";
                var parameter = new { anticipacoin = anticipacion };
                adeudos = connection.Query<Adeudo>(storedProcedure, parameter, commandType: CommandType.StoredProcedure).ToList();

                connection.Close();

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                if(connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            return adeudos;
        }

        public List<Cliente> ObtenerClientePorContrato(string [] Contratos)
        {
            SqlConnection connection = new SqlConnection(SqlConnectionString);
            List<Cliente> clientes = new List<Cliente>();
            try
            {
                connection.Open();
                string contratosString = string.Join(",", Contratos);

                var storedProcedure = "ObtenerUsuariosPorContrato";
                clientes = connection.Query<Cliente>(storedProcedure, new { @ContratosId = contratosString }, commandType: CommandType.StoredProcedure).ToList();
                connection.Close();

            }
            catch (Exception ex)
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            return clientes;
        }

        public void ActualizarReferencia(int Id, string referencia)
        {
            SqlConnection connection = new SqlConnection(SqlConnectionString);
            try
            {
                var storedProcedure = "AgregarReferenciaAPago";
                //DynamicParameters parameters = new DynamicParameters();
                //parameters.Add("@Referencia", referencia, DbType.String, ParameterDirection.Input);
                //parameters.Add("@Id", Id, DbType.Int32, ParameterDirection.Input);

                connection.Execute(storedProcedure, new { Id = Id, Referencia = referencia }, commandType: CommandType.StoredProcedure);
                connection.Close();
            }
            catch (Exception ex)
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }

                throw;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
        }
    }
}
