﻿using RealState.Notification.Dtos;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OpenPay = RealState.Banco;

namespace RealState.Notification
{
    class MensajeSender
    {
        
        static string fromEmail = string.Empty;
        static string emailSubjet = string.Empty;
        static string sendgridApikey = string.Empty;
        static int anticipacion = 0;
        static Sqloperations sql;
        private OpenPay.Banco banco = null;

        //static void Main(string[] args)
        //{
        //    new MensajeSender().Send();
        //}

        /// <summary>
        /// Inicializar parámetros de envío de correo electrónico y días de anticipación para enviar las notificaciones
        /// </summary>
        public MensajeSender()
        {
            sql = new Sqloperations();
            banco = new OpenPay.Banco();
            fromEmail = ConfigurationManager.AppSettings.Get("fromEmail");
            emailSubjet = ConfigurationManager.AppSettings.Get("EmailSubject");
            sendgridApikey = ConfigurationManager.AppSettings.Get("sendGridApiKey");
            anticipacion = Convert.ToInt32(ConfigurationManager.AppSettings.Get("Anticipacion"));
        }

        /// <summary>
        /// Tarea para envío de notificaciones por correo electrónico y SMS.
        /// Funcionamiento:
        /// 1. Se obtienen los adeudos por cliente.
        /// 2. Se recorren cada uno de los adeudos validando que los días para el próximo vencimiento se encuentren dentro de los días de anticipación.
        /// 3. Se valida la estructura del número de teléfono para enviar el SMS.
        /// 4. Se valida la estructura del correo electrónico para enviar el mensaje.
        /// </summary>
        public void Send(string environment)
        {
            var sms = new SMSSender();

            List<Adeudo> todosAdeudos = sql.ObtenerAdeudos(anticipacion);

            /* Begin. generar pagos de open pay*/
            List<Adeudo> adeudos = new List<Adeudo>();
            var adeudosAgrupados = todosAdeudos.GroupBy(x => x.ContratoId);
            foreach(var adeudoItem in adeudosAgrupados)
            {
                var adeudo = adeudoItem.OrderByDescending(x => x.FechaPago).First();
                adeudo.Monto = adeudoItem.Sum(x => x.Total);

                if (!adeudos.Any(x => x.ContratoId == adeudo.ContratoId))
                    adeudos.Add(adeudo);
            }
            
            /*End*/

            var contratos = adeudos.Select(c => c.ContratoId.ToString()).ToArray();
            List<OpenPay.Dto.Cliente> clientesOpenPay = new List<OpenPay.Dto.Cliente>();
            OpenPay.Dto.BancoOpenPay pagoOpenPay = new OpenPay.Dto.BancoOpenPay();

            if (contratos.Length > 0)
            {
                List<Cliente> clientes = sql.ObtenerClientePorContrato(contratos).ToList();

                clientesOpenPay = clientes.Select(item =>
                new OpenPay.Dto.Cliente()
                {
                    Id = item.Id,
                    Nombres = item.Nombres,
                    Apellidos = item.Apellidos,
                    Email = item.Email
                }).ToList();
            }



            foreach (var adeudo in adeudos)
            {
                string guid = string.Empty;
                try
                {
                    if (ValidarCelular(adeudo.Celular))
                    {
                        if (string.IsNullOrEmpty(adeudo.Referencia))
                        {
                            var cliente = clientesOpenPay.Where(x => x.Id == adeudo.ClienteId).FirstOrDefault();
                            guid = Guid.NewGuid().ToString();
                            if (environment == "Release")
                            {
                                pagoOpenPay = banco.GeneraPagoOpenPay(cliente, adeudo.Monto, guid);
                                sql.ActualizarReferencia(adeudo.PagoId, pagoOpenPay.Referencia);
                            }
                            else
                            {
                                sql.ActualizarReferencia(adeudo.PagoId, guid);
                            }

                            if (environment == "Release")
                                sms.enviarMensaje(adeudo, pagoOpenPay.Referencia);
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                try
                {
                    if (ValidarEmail(adeudo.CorreoElectronico))
                    {
                        if (string.IsNullOrEmpty(adeudo.Referencia))
                        {
                            if (environment == "Release")
                                SendEmail(adeudo, pagoOpenPay.Referencia);
                            else if(environment == "Debug")
                                SendEmail(adeudo, guid);
                            else
                                SendFakeEmail(adeudo, guid);
                        }
                    }
                }
                catch (Exception)
                {

                }
                //if ((adeudo.FechaPago - DateTime.Today).Days == anticipacion)
                //{


                //}
            }
        }

        /// <summary>
        /// Tarea para envío de notificaciones por correo electrónico y SMS de pagos recibidos.
        /// <param name="Contrato_Id">Id de contrato</param>
        /// <param name="Email">Correo electrónico del cliente</param>
        /// <param name="Telefono">Teléfono del cliente</param>
        /// <param name="Nombre">Nombre del cliente</param>
        /// <param name="Monto">Monto del pago</param>
        /// </summary>
        public void Send(int Contrato_Id, string Email, string Telefono, string Nombre, decimal Monto)
        {
            var sms = new SMSSender();

            try
            {
                if (ValidarCelular(Telefono))
                {
                    sms.enviarMensaje(Email, Telefono, Nombre, Monto);
                }
            }
            catch (Exception)
            {
            }

            try
            {
                if (ValidarEmail(Email))
                {
                    SendEmail(Email, Telefono, Nombre, Monto);
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Tarea para envío de notificaciones por correo electrónico y SMS de pagos recibidos.
        /// <param name="Contrato_Id">Id de contrato</param>
        /// <param name="Email">Correo electrónico del cliente</param>
        /// <param name="Telefono">Teléfono del cliente</param>
        /// <param name="Nombre">Nombre del cliente</param>
        /// <param name="Lote">Lote del contrato</param>
        /// <param name="Parcela">Parcela del contrato</param>
        /// </summary>
        public void Send(string Contrato_Id, string Email, string Telefono, string Nombre, string Lote, string Parcela)
        {
            var sms = new SMSSender();

            try
            {
                if (ValidarCelular(Telefono))
                {
                    sms.enviarMensaje(Contrato_Id, Email, Telefono, Nombre, Lote, Parcela);
                }
            }
            catch (Exception)
            {
            }

            try
            {
                if (ValidarEmail(Email))
                {
                    SendEmail(Contrato_Id, Email, Telefono, Nombre, Lote, Parcela);
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Invoca las instrucciones necesarias para el envío del correo electronico.
        /// </summary>
        /// <param name="unAdeudo">Corresponde a un objeto de la clase Adeudo que contiene la información necesaria del adeudo del cliente</param>        
        private void SendEmail(Adeudo unAdeudo, string referencia)
        {
            try
            {
                var apiKey = sendgridApikey;
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress(fromEmail, "Desert & Asociados");
                var subject = emailSubjet;
                var to = new EmailAddress(unAdeudo.CorreoElectronico, unAdeudo.Cliente);
                var plainTextContent = string.Format("Tu pago por {0} del contrato {1} esta por vencer. Tienes un adeudo total por {2}. Tu numero de referencia es {3}", unAdeudo.Monto.ToString("C", CultureInfo.CurrentCulture), unAdeudo.Contrato, unAdeudo.Total.ToString("C", CultureInfo.CurrentCulture), referencia);
                var htmlContent = string.Format("Tu pago por {0} del contrato {1} esta por vencer. Tienes un adeudo total por {2}. Tu numero de referencia es {3}", unAdeudo.Monto.ToString("C", CultureInfo.CurrentCulture), unAdeudo.Contrato, unAdeudo.Total.ToString("C", CultureInfo.CurrentCulture), referencia);
                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                var response = client.SendEmailAsync(msg).Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SendFakeEmail(Adeudo unAdeudo, string referencia)
        {
            try
            {
                var htmlContent = string.Format("Tu pago por {0} del contrato {1} esta por vencer. Tienes un adeudo total por {2}. Tu numero de referencia es {3}", unAdeudo.Monto.ToString("C", CultureInfo.CurrentCulture), unAdeudo.Contrato, unAdeudo.Total.ToString("C", CultureInfo.CurrentCulture), referencia);

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.mailtrap.io", 2525);

                mail.From = new MailAddress(fromEmail);
                mail.To.Add(unAdeudo.CorreoElectronico);
                mail.Subject = "Recordatorio";
                mail.Body = htmlContent;

                SmtpServer.Credentials = new System.Net.NetworkCredential("d09df185ed422e", "042571191d3097");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);

                //var apiKey = sendgridApikey;
                //var client = new SendGridClient(apiKey);
                //var from = new EmailAddress(fromEmail, "Desert & Asociados");
                //var subject = emailSubjet;
                //var to = new EmailAddress(unAdeudo.CorreoElectronico, unAdeudo.Cliente);
                //var plainTextContent = string.Format("Tu pago por {0} del contrato {1} esta por vencer. Tienes un adeudo total por {2}. Tu numero de referencia es {3}", unAdeudo.Monto.ToString("C", CultureInfo.CurrentCulture), unAdeudo.Contrato, unAdeudo.Total.ToString("C", CultureInfo.CurrentCulture), referencia);
                //var htmlContent = string.Format("Tu pago por {0} del contrato {1} esta por vencer. Tienes un adeudo total por {2}. Tu numero de referencia es {3}", unAdeudo.Monto.ToString("C", CultureInfo.CurrentCulture), unAdeudo.Contrato, unAdeudo.Total.ToString("C", CultureInfo.CurrentCulture), referencia);
                //var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                //var response = client.SendEmailAsync(msg).Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Invoca las instrucciones necesarias para el envío del correo electronico.
        /// </summary>
        private void SendEmail(string Email, string Telefono, string Nombre, decimal Monto)
        {
            try
            {
                var apiKey = sendgridApikey;
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress(fromEmail, "Desert & Asociados");
                var subject = emailSubjet;
                var to = new EmailAddress(Email, Nombre);
                var plainTextContent = string.Format("Tu pago por {0} fue aplicado correctamente", Monto.ToString("C", CultureInfo.CurrentCulture));
                var htmlContent = string.Format("Tu pago por {0} fue aplicado correctamente", Monto.ToString("C", CultureInfo.CurrentCulture));
                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                var response = client.SendEmailAsync(msg).Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Invoca las instrucciones necesarias para el envío del correo electronico.
        /// </summary>
        private void SendEmail(string Contrato_Id, string Email, string Telefono, string Nombre, string Lote, string Parcela)
        {
            try
            {
                var apiKey = sendgridApikey;
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress(fromEmail, "Desert & Asociados");
                var subject = emailSubjet;
                var to = new EmailAddress(Email, Nombre);
                var plainTextContent = string.Format("{0}, tu contrato {1} para el Lote {2} ha sido creado correctamente", Nombre, Contrato_Id, Lote);
                var htmlContent = string.Format("{0}, tu contrato {1} para el Lote {2} ha sido creado correctamente", Nombre, Contrato_Id, Lote);
                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                var response = client.SendEmailAsync(msg).Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Validar la estructura del número telefónico para confirmar que es un valor válido
        /// </summary>
        /// <param name="Celular">Representa el número de teléfono del cliente</param>
        private bool ValidarCelular(string Celular)
        {
            try
            {
                const string MatchPhonePattern = @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}";
                Regex rx = new Regex(MatchPhonePattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                MatchCollection matches = rx.Matches(Celular);
                return matches.Count > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Validar la estructura del correo electrónico para confirmar que es un valor válido
        /// </summary>
        /// <param name="Email">Representa el correo electrónico del cliente</param>
        private bool ValidarEmail(string Email)
        {
            try
            {
                MailAddress m = new MailAddress(Email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
