﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace RealState.Notification
{
    public class Notification
    {
        private Sqloperations operations;

        /// <summary>
        /// Inicia el envío de mensajes
        /// </summary>
        /// <param name="sqlConnection"></param>
        public void SendNotifications(string sqlConnection, string environment)
        {
            if (string.IsNullOrEmpty(sqlConnection))
                throw new Exception("No se ha inicializado la cadena de conexion SQL");

            operations = new Sqloperations();
            try
            {
                new MensajeSender().Send(environment);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// Inicia el envío de mensajes
        /// </summary>
        /// <param name="sqlConnection"></param>
        public void SendNotifications(string sqlConnection, int Contrato_Id, string Email, string Telefono, string Nombre, decimal Monto)
        {
            if (string.IsNullOrEmpty(sqlConnection))
                throw new Exception("No se ha inicializado la cadena de conexion SQL");

            operations = new Sqloperations();
            try
            {
                new MensajeSender().Send(Contrato_Id, Email, Telefono, Nombre, Monto);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// Inicia el envío de mensajes
        /// </summary>
        /// <param name="sqlConnection"></param>
        public void SendNotifications(string sqlConnection, string Contrato_Id, string Email, string Telefono, string Nombre, string Lote, string Parcela)
        {
            if (string.IsNullOrEmpty(sqlConnection))
                throw new Exception("No se ha inicializado la cadena de conexion SQL");

            operations = new Sqloperations();
            try
            {
                new MensajeSender().Send(Contrato_Id, Email, Telefono, Nombre, Lote, Parcela);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void SendEmailNotification()
        {

        }
    }
}
