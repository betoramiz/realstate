﻿using System;
using RealState.Notification.Dtos;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
using System.Globalization;
using System.Configuration;

namespace RealState.Notification
{
    public class SMSSender
    {
        readonly String URLSERVICIO = ConfigurationManager.AppSettings.Get("URLSERVICIO");
        readonly String CLIENTE = ConfigurationManager.AppSettings.Get("CLIENTE");
        readonly String PASSWORD = ConfigurationManager.AppSettings.Get("PASSWORD");
        readonly String USER = ConfigurationManager.AppSettings.Get("USER");

        /// <summary>
        /// Tarea para envío de notificaciones por SMS.
        /// Funcionamiento:
        /// 1. A partir de la URL del servicio de envío de SMS crea un objeto de tipo HttpClient.
        /// 2. Se construye la estructura de Key/Value con los parámetros definidos por el API de CalixtaOnDemand [https://www.calixtaondemand.com/Controller.php/__a/api.select/tipo//x/515/y/3beba/ax_obj_cache_lifetime/5/]
        /// 3. Se invoca la petición asíncrona del envío de SMS y se devuelve el resultado.
        /// </summary>
        /// <param name="unAdeudo">Corresponde a un objeto de la clase Adeudo que contiene la información necesaria del adeudo del cliente</param>
        public void enviarMensaje(Adeudo unAdeudo, string OpenpayReferencia)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URLSERVICIO);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                List<KeyValuePair<string, string>> pairs = new List<KeyValuePair<string, string>>();

                pairs.Add(new KeyValuePair<string, string>("cte", CLIENTE));
                pairs.Add(new KeyValuePair<string, string>("encpwd", PASSWORD));
                pairs.Add(new KeyValuePair<string, string>("email", USER));
                pairs.Add(new KeyValuePair<string, string>("msg", string.Format("Tu pago por {0} del contrato {1} esta por vencer. Tienes un adeudo total por {2}", unAdeudo.Monto.ToString("C", CultureInfo.CurrentCulture), unAdeudo.Contrato, unAdeudo.Total.ToString("C", CultureInfo.CurrentCulture))));
                pairs.Add(new KeyValuePair<string, string>("referencia", string.Format("Referencia: {0}", OpenpayReferencia)));
                pairs.Add(new KeyValuePair<string, string>("numtel", unAdeudo.Celular));
                pairs.Add(new KeyValuePair<string, string>("mtipo", "SMS"));
                pairs.Add(new KeyValuePair<string, string>("idivr", "125"));
                pairs.Add(new KeyValuePair<string, string>("auxiliar", "cta2134"));

                FormUrlEncodedContent content = new FormUrlEncodedContent(pairs);

                var response = client.PostAsync(string.Empty, content).Result;
                var responseString = response.Content.ReadAsStringAsync();
                client.Dispose();
                //return new ExitDisposable(responseString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Tarea para envío de notificaciones por SMS.
        /// Funcionamiento:
        /// 1. A partir de la URL del servicio de envío de SMS crea un objeto de tipo HttpClient.
        /// 2. Se construye la estructura de Key/Value con los parámetros definidos por el API de CalixtaOnDemand [https://www.calixtaondemand.com/Controller.php/__a/api.select/tipo//x/515/y/3beba/ax_obj_cache_lifetime/5/]
        /// 3. Se invoca la petición asíncrona del envío de SMS y se devuelve el resultado.
        /// </summary>
        public void enviarMensaje(string Email, string Telefono, string Nombre, decimal Monto)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URLSERVICIO);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                List<KeyValuePair<string, string>> pairs = new List<KeyValuePair<string, string>>();

                pairs.Add(new KeyValuePair<string, string>("cte", CLIENTE));
                pairs.Add(new KeyValuePair<string, string>("encpwd", PASSWORD));
                pairs.Add(new KeyValuePair<string, string>("email", USER));
                pairs.Add(new KeyValuePair<string, string>("msg", string.Format("Tu pago por {0} fue aplicado correctamente", Monto.ToString("C", CultureInfo.CurrentCulture))));
                pairs.Add(new KeyValuePair<string, string>("numtel", Telefono));
                pairs.Add(new KeyValuePair<string, string>("mtipo", "SMS"));
                pairs.Add(new KeyValuePair<string, string>("idivr", "125"));
                pairs.Add(new KeyValuePair<string, string>("auxiliar", "cta2134"));

                FormUrlEncodedContent content = new FormUrlEncodedContent(pairs);

                var response = client.PostAsync(string.Empty, content).Result;
                var responseString = response.Content.ReadAsStringAsync();
                client.Dispose();
                //return new ExitDisposable(responseString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Tarea para envío de notificaciones por SMS.
        /// Funcionamiento:
        /// 1. A partir de la URL del servicio de envío de SMS crea un objeto de tipo HttpClient.
        /// 2. Se construye la estructura de Key/Value con los parámetros definidos por el API de CalixtaOnDemand [https://www.calixtaondemand.com/Controller.php/__a/api.select/tipo//x/515/y/3beba/ax_obj_cache_lifetime/5/]
        /// 3. Se invoca la petición asíncrona del envío de SMS y se devuelve el resultado.
        /// </summary>
        public void enviarMensaje(string Contrato_Id, string Email, string Telefono, string Nombre, string Lote, string Parcela)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URLSERVICIO);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                List<KeyValuePair<string, string>> pairs = new List<KeyValuePair<string, string>>();

                pairs.Add(new KeyValuePair<string, string>("cte", CLIENTE));
                pairs.Add(new KeyValuePair<string, string>("encpwd", PASSWORD));
                pairs.Add(new KeyValuePair<string, string>("email", USER));
                pairs.Add(new KeyValuePair<string, string>("msg", string.Format("{0}, tu contrato {1} para el Lote {2} ha sido creado correctamente", Nombre, Contrato_Id, Lote)));
                pairs.Add(new KeyValuePair<string, string>("numtel", Telefono));
                pairs.Add(new KeyValuePair<string, string>("mtipo", "SMS"));
                pairs.Add(new KeyValuePair<string, string>("idivr", "125"));
                pairs.Add(new KeyValuePair<string, string>("auxiliar", "cta2134"));

                FormUrlEncodedContent content = new FormUrlEncodedContent(pairs);

                var response = client.PostAsync(string.Empty, content).Result;
                var responseString = response.Content.ReadAsStringAsync();
                client.Dispose();
                //return new ExitDisposable(responseString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


/// <summary>
/// Clase para devolver una salida asíncrona
/// </summary>
class ExitDisposable : IDisposable
    {
        private readonly object obj;
        public ExitDisposable(object obj) { this.obj = obj; }
        public void Dispose() { Monitor.Exit(this.obj); }
    }
}
