﻿using Openpay;
using Openpay.Entities;
using Openpay.Entities.Request;
using RealState.Banco.Dto;
using System;
using System.Configuration;

namespace RealState.Banco
{
    public class Banco
    {
        public string OpenPayId
        {
            get
            {
                return ConfigurationManager.AppSettings["OpenPayApiId"];
            }
        }

        public string OpenPayKey
        {
            get
            {
                return ConfigurationManager.AppSettings["OpenPayPrivateKey"];
            }
        }

        public BancoOpenPay GeneraPagoOpenPay(Cliente cliente, decimal monto, string ordenId)
        {
            OpenpayAPI api = new OpenpayAPI(OpenPayKey, OpenPayId);
            ChargeRequest request = new ChargeRequest();
            request.Method = "bank_account";
            request.Amount = monto;
            request.Description = "Cargo con banco";
            request.OrderId = $"oid-{ordenId}";
            request.Customer = new Customer()
            {
                Id = cliente.Id,
                LastName = cliente.Apellidos,
                Name = cliente.Nombres,
                Email = cliente.Email

            };
            var charge = new Charge();

            try
            {
                charge = api.ChargeService.Create(request);
                //BancoOpenPay chargeMapped = Mapper.Map<Charge, BancoOpenPay>(charge);
                BancoOpenPay chargeMapped = new BancoOpenPay() {
                    Amount = charge.Amount,
                    Authorization = charge.Authorization,
                    CreationDate = charge.CreationDate.HasValue  == true ? charge.CreationDate.Value : DateTime.Today,
                    Description = charge.Description,
                    ErrorMessage = charge.ErrorMessage,
                    Id = charge.Id,
                    Method = charge.Method,
                    OperationType = charge.OperationType,
                    OrderId = charge.OrderId,
                    //PaymentMethod = charge.PaymentMethod,
                    Referencia = charge.PaymentMethod.Name,
                    Status = charge.Status
                };
                return chargeMapped;
            }
            catch (Exception ex)
            {
                return new BancoOpenPay();
            }
        }
    }
}
