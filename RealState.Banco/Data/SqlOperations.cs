﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealState.Banco.Data
{
    public class SqlOperations
    {
        private string _sqlConnectionString;
        public string sqlConnectionString
        {
            get
            {
                return _sqlConnectionString;
            }

            private set
            {
                _sqlConnectionString = ConfigurationManager.ConnectionStrings["RealStateConnection"].ConnectionString;
            }
        }

        public SqlOperations()
        {
            
        }
    }
}
