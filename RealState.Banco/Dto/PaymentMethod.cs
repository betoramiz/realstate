﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealState.Banco.Dto
{
    public class PaymentMethod
    {
        public string Type { get; set; }
        public string BankName { get; set; }
        public string Agreement { get; set; }
        public string CLABE { get; set; }
        public string Name { get; set; }
    }
}
